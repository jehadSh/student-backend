<?php
return [
    'paginate' => [
        \App\Models\Address::class        => 10,
        \App\Models\CallNumber::class     => 10,
        \App\Models\Day::class            => 10,
        \App\Models\DaySemester::class    => 10,
        \App\Models\EducationLevel::class => 10,
        \App\Models\Group::class          => 10,
        \App\Models\GroupHistory::class   => 10,
        \App\Models\GroupStudent::class   => 10,
        \App\Models\Mark::class           => 10,
        \App\Models\Member::class         => 10,
        \App\Models\Permission::class     => 10,
        \App\Models\PermissionRole::class => 10,
        \App\Models\Role::class           => 10,
        \App\Models\SavedMark::class      => 10,
        \App\Models\SchoolClass::class    => 10,
        \App\Models\Semester::class       => 10,
        \App\Models\SemesterGroup::class  => 10,
        \App\Models\Student::class        => 10,
        \App\Models\StudentCheckIn::class => 10,
        \App\Models\Teacher::class        => 10,
        \App\Models\TeacherReview::class  => 10,
        \App\Models\TimeType::class       => 10,
        \App\Models\User::class           => 10,
        \App\Models\UserRole::class       => 10,
    ]
];
