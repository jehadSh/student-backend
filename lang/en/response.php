<?php

return [

    'deleted' => 'Deleted successfully',
    'updated' => 'updated successfully',
    'created' => 'Created successfully',
    'failed' => 'Failed',
    'success' => 'Success',
    'wrong' => 'Some thing went wrong',
    'unauthorized' => 'Unauthorized',
    'not_found' => 'Not found',
    'validation_error' => 'Validation error',
];
