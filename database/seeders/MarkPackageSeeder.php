<?php

namespace Database\Seeders;

use App\Models\MarkPackage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MarkPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $marks_package = [
            [
                "id"   => 1,
                "name" => 'دوام',
            ],
            [
                "id"   => 2,
                "name" => 'تسميع',
            ],
            [
                "id"   => 3,
                "name" => 'مجد 1 ',
            ],
            [
                "id"   => 4,
                "name" => 'مجد ما بعت الصورة',
            ],
            [
                "id"   => 5,
                "name" => 'مجد 2',
            ],
            [
                "id"   => 6,
                "name" => 'مجد حمامي مقصر',
            ],
            [
                "id"   => 7,
                "name" => 'خلي ابو عبيدة يعدلون',
            ],

        ];

        MarkPackage::insert($marks_package);
    }
}
