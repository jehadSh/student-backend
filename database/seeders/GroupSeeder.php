<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Create a group
        Group::create([
            'name'                => 'Zubair',
            'is_active'           => true,
            'serial_number'       => "777",
            'teacher_id'          => 1, // Example teacher ID
            'merge_group_id'      => null,
            'separate_group_id'   => null,
            'school_class_id'     => 1, // Example school class ID
            'current_semester_id' => 1, // Example school class ID

        ]);
    }
}
