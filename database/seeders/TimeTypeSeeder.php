<?php

namespace Database\Seeders;

use App\Enums\SemesterDaysEnum;
use App\Models\TimeType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TimeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $timeType = [
            [
                "days" => SemesterDaysEnum::winter_1->value,
                "time" => 'من العصر لل 6 ',
            ],
            [
                "days" => SemesterDaysEnum::Saturday->value,
                "time" => 'من الضهر لل 3 ',
            ],

        ];

        TimeType::insert($timeType);
    }
}
