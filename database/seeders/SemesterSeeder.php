<?php

namespace Database\Seeders;

use App\Models\Semester;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SemesterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Create a semester
        Semester::create([
            'name'         => 'Summer 2024',
            'start_date'   => '2023-01-01',
            'end_date'     => '2023-05-31',
            'is_active'    => true,
            // 'days'         => 'one',
            'time_type_id' => 1, // Example time type ID
        ]);
    }
}
