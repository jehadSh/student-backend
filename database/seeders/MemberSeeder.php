<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Member::create([
            'first_name' => 'John',
            'last_name'  => 'Doe',
            'birth_date' => '1995-05-10',
        ]);
    }
}
