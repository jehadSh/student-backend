<?php

namespace Database\Seeders;

use App\Enums\MarkEnum;
use App\Models\Mark;
use Illuminate\Database\Seeder;

class MarkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $marks = [
            [
                "id"                => 1,
                "serial_number"     => 'M 0001',
                'type'              => "Attendance",
                'once_a_day'        => true,
                "description"       => 'This Mark Is Given To The Students That Come To The Lesson Today.',
                "max_mark"          => '10',
                "min_mark"          => null,
                "is_flixable"       => 0,
                "mark_package_id"   => 1,
            ],
            [
                "id"                => 2,
                "serial_number" => 'M 0002',
                "description" => 'dasd',
                'type'       => MarkEnum::FROM_TEACHER,
                'once_a_day' => false,
                "max_mark"   => '10',
                "min_mark"   => null,
                "is_flixable"   => 0,
                "mark_package_id"   => 1,
            ],
            [
                "id"                => 3,
                "serial_number" => 'M 0003',
                "description" => 'wasd',
                'type'       => MarkEnum::FROM_SUPERVISER,
                'once_a_day' => false,
                "max_mark"   => '25',
                "min_mark"   => null,
                "is_flixable"   => 0,
                "mark_package_id"   => 1,
            ],
            [
                "id"                => 4,
                "serial_number" => 'M 0004',
                "description" => 'fdsa',
                'type'       => MarkEnum::PART_TEST,
                'once_a_day' => false,
                "max_mark"   => '100',
                "min_mark"   => '60',
                "is_flixable"   => 1,
                "mark_package_id"   => 1,

            ],
            [
                "id"                => 5,
                "serial_number" => 'M 0005',
                "description" => 'dasd',
                'type'       => MarkEnum::RECITE_5_PAGES,
                'once_a_day' => false,
                "max_mark"   => '25',
                "min_mark"   => null,
                "is_flixable"   => 0,
                "mark_package_id"   => 1,
            ],
            [
                "id"                => 6,
                "serial_number" => 'M 0006',
                "description" => 'efda',
                'type'       => MarkEnum::GOOD_BEHAVIOR,
                'once_a_day' => false,
                "max_mark"   => '10',
                "min_mark"   => null,
                "is_flixable"   => 0,
                "mark_package_id"   => 1,
            ],
        ];
        Mark::insert($marks);
    }
}
