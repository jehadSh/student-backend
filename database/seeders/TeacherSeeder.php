<?php

namespace Database\Seeders;

use App\Models\Member;
use App\Models\Teacher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Create a member
        $member = Member::create([
            'first_name' => 'Alice',
            'last_name'  => 'Smith',
            'birth_date' => '1980-08-15',
        ]);
        $member->address()->create([
            'place_name'        => 'maza',
            'specific_location' => 'jehad'
        ]);

        $member->callNumbers()->create([
            'phone_number' => 'maza',
            'owner_name'   => 'jehad',
            // 'tell_number'    => 'jehad',
            'whatsapp'     => 1,
            'call'         => 0,
            'member_id'    => $member->id,

        ]);

        // Create a teacher associated with the member
        Teacher::create([
            'type'      => 'teacher',
            'member_id' => $member->id,
            // 'education_level_id' => 1, // Example education level ID
        ]);
    }
}
