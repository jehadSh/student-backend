<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(SchoolClassSeeder::class);
        $this->call(EducationLevelSeeder::class);
        $this->call(TimeTypeSeeder::class);
        $this->call(MemberSeeder::class);
        $this->call(TeacherSeeder::class);
        $this->call(SemesterSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(StudentSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MarkPackageSeeder::class);
        $this->call(MarkSeeder::class);
    }
}
