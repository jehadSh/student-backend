<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = [
            [
                "name" => 'jehad',
                "email" => 'jehad@gmail.com',
                'password' => Hash::make('12345678'),
                "is_active" => '1',

            ],
        ];

        User::insert($user);
    }
}
