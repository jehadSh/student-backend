<?php

namespace Database\Seeders;

use App\Models\SchoolClass;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SchoolClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $school = [
            ["name" => 'صف اول'],
            ["name" => 'صف ثاني'],
            ["name" => 'صف ثالث '],
            ["name" => 'صف رابع '],
            ["name" => 'صف خامس'],
            ["name" => 'صف سادس'],
            ["name" => 'صف سابع'],
            ["name" => 'صف ثامن'],
            ["name" => 'صف تاسع'],
            ["name" => 'صف عاشر'],
            ["name" => 'صف حادي عشر'],
            ["name" => 'صف بكالوريا'],
        ];

        SchoolClass::insert($school);
    }
}
