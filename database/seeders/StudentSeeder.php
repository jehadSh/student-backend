<?php

namespace Database\Seeders;

use App\Models\Member;
use App\Models\Student;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Create a member
        $member = Member::create([
            'first_name' => 'jehad',
            'last_name'  => 'zain',
            'birth_date' => '1999-09-14',
        ]);
        // Create a student associated with the member
        Student::create([
            'father_name'      => 'khaled',
            'father_work'      => 'soldier',
            'current_group_id' => 1,
            'is_active' => 1,
            'member_id'        => $member->id,
        ]);

        $member = Member::create([
            'first_name' => 'زيد',
            'last_name'  => 'خضرو',
            'birth_date' => '2013-05-10',
        ]);
        // Create a student associated with the member
        Student::create([
            'father_name'      => 'احمد',
            'member_id'        => $member->id,
            'father_work'      => 'مهندس',
            'current_group_id' => 1,
            'is_active' => 1,
        ]);
    }
}
