<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->boolean('is_active')->default(true);
            $table->string('serial_number')->unique();

            $table->timestamps();

            $table->foreignId('teacher_id')->nullable()->references('id')->on('teachers');
            $table->foreignId('merge_group_id')->nullable()->references('id')->on('groups');
            $table->foreignId('separate_group_id')->nullable()->references('id')->on('groups');
            $table->foreignId('school_class_id')->references('id')->on('school_classes');
            $table->foreignId('current_semester_id')->references('id')->on('semesters');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('groups');
    }
};
