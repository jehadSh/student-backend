<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('group_histories', function (Blueprint $table) {
            $table->id();
            $table->date('from');
            $table->date('to');
            $table->longText('school_group_ids');
            $table->timestamps();

            $table->foreignId('semester_group_id')->references('id')->on('semester_groups');
            $table->foreignId('teacher_id')->references('id')->on('teachers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('group_histories');
    }
};
