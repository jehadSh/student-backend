<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('call_numbers', function (Blueprint $table) {
            $table->id();
            $table->string('phone_number');
            $table->string('owner_name');
            $table->boolean('whatsapp')->default(0);
            $table->boolean('call')->default(0);
            // $table->string('tell_number');
            // $table->enum('type', getEnumValues(\App\Enums\TypeCallNumberEnum::class));

            $table->timestamps();

            $table->foreignId('member_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('call_numbers');
    }
};
