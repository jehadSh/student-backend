<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('saved_marks', function (Blueprint $table) {
            $table->id();
            $table->integer('value');
            $table->date('date');
            $table->timestamps();

            $table->foreignId('student_id')->references('id')->on('students');
            $table->foreignId('semester_id')->references('id')->on('semesters');
            $table->foreignId('mark_id')->references('id')->on('marks');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('saved_marks');
    }
};
