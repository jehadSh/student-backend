<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('student_check_ins', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->boolean('is_exist');
            $table->timestamps();

            $table->foreignId('student_id')->references('id')->on('students');
            $table->foreignId('group_id')->references('id')->on('groups');
            $table->foreignId('semester_id')->references('id')->on('semesters');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('student_check_ins');
    }
};
