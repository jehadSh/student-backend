<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('day_semesters', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('semester_id')->references('id')->on('semesters');
            $table->foreignId('day_id')->references('id')->on('days');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('day_semesters');
    }
};
