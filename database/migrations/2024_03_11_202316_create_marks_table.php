<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('marks', function (Blueprint $table) {
            $table->id();
            $table->string('serial_number')->unique();
            $table->string('description')->nullable();
            $table->boolean('once_a_day');
            // $table->enum('type', getEnumValues(\App\Enums\MarkEnum::class));
            $table->string('type')->nullable();
            $table->integer('max_mark');
            $table->integer('min_mark')->nullable();
            $table->boolean('is_flixable');

            $table->foreignId('mark_package_id')->references('id')->on('mark_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('marks');
    }
};
