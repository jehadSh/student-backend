<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\GroupStudentController;
use App\Http\Controllers\SchoolClassController;
use App\Http\Controllers\SemesterController;
use App\Http\Controllers\SemesterGroupController;
use App\Http\Controllers\StudentCheckInController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;

//use App\Http\Controllers\EducationLevelController;
use App\Http\Controllers\TimeTypeController;
use App\Http\Controllers\MarkController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\CallNumberController;
use App\Http\Controllers\MarkPackageController;
use App\Http\Controllers\SavedMarkController;


Route::post("/register", [AuthController::class, 'register']);
Route::post("/login", [AuthController::class, 'login']);

// Route::group(['middleware' => 'auth:sanctum'], function () {
Route::post("/logout", [AuthController::class, 'logout']);

Route::prefix('member')->controller(MemberController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{member}', 'show');
    Route::put('update/{member}', 'update');
    Route::delete('delete/{member}', 'destroy');
});

Route::prefix('student')->controller(StudentController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('all-with-in-active', 'allWithInActive');
    Route::post('active-student/{group}', 'activeStudent');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{student}', 'show');
    Route::put('update/{student}', 'update');
    Route::delete('delete/{student}', 'destroy');
    Route::delete('force-delete/{student}', 'force_destroy');
});

Route::prefix('teacher')->controller(TeacherController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{teacher}', 'show');
    Route::put('update/{teacher}', 'update');
    Route::delete('delete/{teacher}', 'destroy');
});

Route::prefix('semester')->controller(SemesterController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{semester}', 'show');
    Route::put('update/{semester}', 'update');
    Route::delete('delete/{semester}', 'destroy');
});

Route::prefix('group')->controller(GroupController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{group}', 'show');
    Route::put('update/{group}', 'update');
    Route::delete('delete/{group}', 'destroy');
    Route::get('all_with_trashed', 'all_with_trashed');
    Route::post('assign-student-to-group/{group}', 'AssignStudentToGroup');
    Route::post('give-group-reward/{group}', 'giveGroupReward');

});

Route::prefix('semester-group')->controller(SemesterGroupController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{}', 'show');
    Route::put('update/{}', 'update');
    Route::delete('delete/{}', 'destroy');
});

Route::prefix('group-student')->controller(GroupStudentController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'createMany');
    Route::get('show/{groupStudent}', 'show');
    Route::put('update/{groupStudent}', 'update');
    Route::delete('delete/{groupStudent}', 'destroy');
});

Route::prefix('student-checkIn')->controller(StudentCheckInController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{studentCheckIn}', 'show');
    Route::put('update/{studentCheckIn}', 'update');
    Route::delete('delete/{studentCheckIn}', 'destroy');

    Route::post('group-checkIn', 'GroupCheckIn');
});

Route::prefix('mark')->controller(MarkController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{mark}', 'show');
    Route::put('update/{mark}', 'update');
    Route::delete('delete/{mark}', 'destroy');
});

Route::prefix('save-mark')->controller(SavedMarkController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{saveMark}', 'show');
    Route::put('update/{saveMark}', 'update');
    Route::delete('delete/{saveMark}', 'destroy');

    Route::post('add-mark', 'addMarkToStudent');
});

Route::prefix('address')->controller(AddressController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{}', 'show');
    Route::put('update/{}', 'update');
    Route::delete('delete/{}', 'destroy');
});

Route::prefix('call-number')->controller(CallNumberController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{}', 'show');
    Route::put('update/{}', 'update');
    Route::delete('delete/{}', 'destroy');
});

Route::prefix('school-class')->controller(SchoolClassController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{schoolClass}', 'show');
    Route::put('update/{schoolClass}', 'update');
    Route::delete('delete/{schoolClass}', 'destroy');
});

Route::prefix('time-type')->controller(TimeTypeController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
    Route::post('create', 'store');
    Route::get('show/{timeType}', 'show');
    Route::put('update/{timeType}', 'update');
    Route::delete('delete/{timeType}', 'destroy');
});

Route::prefix('mark-package')->controller(MarkPackageController::class)->group(function () {
    Route::get('all', 'all');
    Route::get('/', 'index');
});

// });
