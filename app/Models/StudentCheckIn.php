<?php

namespace App\Models;

use App\Filters\FilterTypes\BooleanFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class StudentCheckIn extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'student_check_ins';

    protected $fillable = [
        'date',
        'is_exist',
        'student_id',
        'group_id',
        'semester_id',
    ];


    protected $filterable = [
        'student_id'      => TextFilter::class,
        'group_id'        => TextFilter::class,
        'semester_id'     => TextFilter::class,
        'is_exist'        => BooleanFilter::class,

    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }
    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
