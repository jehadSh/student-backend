<?php

namespace App\Models;

use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class DaySemester extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'day_semesters';

    protected $fillable = [
        'semester_id',
        'day_id',
    ];

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function day()
    {
        return $this->belongsTo(Day::class);
    }
    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
//            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }


}
