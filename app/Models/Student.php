<?php

namespace App\Models;

use App\Filters\FilterTypes\SelectFilter;
use App\Filters\FilterTypes\SpecificTextFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class Student extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'students';

    protected $fillable = [
        'father_name',
        'father_work',
        'notes',
        'current_group_id',
        'is_active',
        'member_id',
    ];
    protected $filterable = [
        'father_name'     => TextFilter::class,
        'father_work'      => TextFilter::class,
        'current_group_id'   => SelectFilter::class,
        'grant_date'      => SelectFilter::class,
        'employee_id'     => SelectFilter::class,
    ];


    public $modelRelations = [
        'currentGroup'  => Group::class,
        'member'  => Member::class,
    ];

    protected static function booted()
    {
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('is_active', '=', 1);
        });
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_students');
    }

    public function currentGroup()
    {
        return $this->belongsTo(Group::class, 'current_group_id');
    }

    public function reviews()
    {
        return $this->hasMany(TeacherReview::class);
    }

    public function marks()
    {
        return $this->hasMany(SavedMark::class);
    }

    public function check_ins()
    {
        return $this->hasMany(StudentCheckIn::class);
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($student) {
            $student->marks()->delete();
            $student->reviews()->delete();
            $student->check_ins()->delete();
            $student->groups()->detach();
        });
    }
}
