<?php

namespace App\Models;

use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use App\Traits\PerPageTrait;
use App\Traits\ResourcePaginatorTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class SchoolClass extends Model
{
    use HasFactory;
    use PerPageTrait;
    use HasFilterTrait;


    protected $table = 'school_classes';

    protected $fillable = [
        'name',
    ];

    protected $filterable = [
        'name' => TextFilter::class,

    ];

    public function groups()
    {
        return $this->hasMany(Group::class, 'school_class_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'name' => [$required],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
