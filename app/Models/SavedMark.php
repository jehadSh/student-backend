<?php

namespace App\Models;

use App\Filters\FilterTypes\DateRangeFilter;
use App\Filters\FilterTypes\SelectFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class SavedMark extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'saved_marks';


    protected $fillable = [
        'value',
        'date',
        'student_id',
        'semester_id',
        'mark_id',
    ];

    protected $filterable = [
        'date'            => DateRangeFilter::class,
        'mark_id'         => SelectFilter::class,
        'semester_id'     => SelectFilter::class,
        'student_id'      => SelectFilter::class,
        'value'           => SelectFilter::class,
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function mark()
    {
        return $this->belongsTo(Mark::class, 'mark_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
