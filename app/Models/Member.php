<?php

namespace App\Models;

use App\Filters\FilterTypes\DateRangeFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Member extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'members';

    protected $fillable = [
        'first_name',
        'last_name',
        'birth_date',
    ];

    protected $casts = [
        'birth_date' => 'datetime',
    ];

    protected $filterable = [
        'first_name' => TextFilter::class,
        'last_name'  => TextFilter::class,
        'birth_date' => DateRangeFilter::class,
    ];

    public $modelRelations = [
        'address'  => Address::class,
        'callNumbers'  => CallNumber::class,
    ];

    public static array $pluralRelationships = [
        'callNumbers',
        'address',
    ];
    public function student()
    {
        return $this->hasOne(Student::class);
    }

    public function teacher()
    {
        return $this->hasOne(Teacher::class);
    }

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    public function callNumbers()
    {
        return $this->hasMany(CallNumber::class);
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
            //            'department_type_id' => [$required, 'exists:department_types,id'],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
