<?php

namespace App\Models;

use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class MarkPackage extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'mark_packages';

    protected $fillable = [
        'name',
    ];
    protected $filterable = [
        'name'     => TextFilter::class,
    ];
    public function marks()
    {
        return $this->hasMany(Mark::class, 'mark_package_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
