<?php

namespace App\Models;

use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Teacher extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'teachers';

    protected $fillable = [
        'type',
        'member_id',
        // 'education_level_id',
    ];

    protected $filterable = [
        'type'                => TextFilter::class,
    ];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    // public function educationLevel()
    // {
    //     return $this->belongsTo(EducationLevel::class);
    // }

    public function group()
    {
        return $this->hasOne(Group::class);
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
//            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }

}
