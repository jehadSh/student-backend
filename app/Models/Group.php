<?php

namespace App\Models;

use App\Filters\FilterTypes\BooleanFilter;
use App\Filters\FilterTypes\SelectFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use App\Traits\PerPageTrait;
use App\Traits\HasFilterTrait;
use App\Filters\FilterTypes\TextFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

class Group extends Model
{
    use HasFactory, PerPageTrait, HasFilterTrait;

    protected $table = 'groups';

    protected $fillable = [
        'name',
        'is_active',
        'serial_number',
        'teacher_id',
        'merge_group_id',
        'separate_group_id',
        'school_class_id',
        'current_semester_id',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];


    protected $filterable = [
        'name'                => TextFilter::class,
        'is_active'           => BooleanFilter::class,
        'teacher_id'          => SelectFilter::class,
        'school_class_id'     => SelectFilter::class,
        'current_semester_id' => SelectFilter::class,
    ];

    protected $modelRelations = [
        'teacher'     => Teacher::class,
        'student'     => Student::class,
        'schoolClass' => SchoolClass::class,
    ];

    protected static function booted()
    {
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('is_active', '=', 1);
        });
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function mergeGroup()
    {
        return $this->belongsTo(Group::class, 'merge_group_id');
    }

    public function separateGroup()
    {
        return $this->belongsTo(Group::class, 'separate_group_id');
    }

    public function schoolClass()
    {
        return $this->belongsTo(SchoolClass::class, 'school_class_id');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'group_students');
    }

    public function current_semester()
    {
        return $this->belongsTo(Semester::class, 'current_semester_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'name'              => [$required, 'string'],
            'is_active'         => [$required, 'boolean',],
            'serial_number'     => [$required, 'string',],
            'teacher_id'        => [$required, 'exists:teachers,id',],
            'merge_group_id'    => ['nullable', 'exists:groups,id',],
            'separate_group_id' => ['nullable', 'exists:groups,id',],
            'school_class_id'   => [$required, 'exists:school_classes,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
