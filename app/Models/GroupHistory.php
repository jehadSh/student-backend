<?php

namespace App\Models;

use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class GroupHistory extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'group_histories';

    protected $fillable = [
        'from',
        'to',
        'school_group_ids',
        'semester_group_id',
        'teacher_id',
    ];

    protected $casts = [
        'from' => 'date',
        'to' => 'date',
        'school_group_ids' => 'array',
    ];

    public function semesterGroup()
    {
        return $this->belongsTo(SemesterGroup::class, 'semester_group_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }
    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
//            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }

}
