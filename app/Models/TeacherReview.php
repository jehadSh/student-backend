<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class TeacherReview extends Model
{
    use HasFactory;

    protected $table = 'teacher_reviews';

    protected $fillable = [
        'is_positive',
        'note',
        'review',
        'teacher_id',
        'student_id',
    ];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
//            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }

}
