<?php

namespace App\Models;

use App\Filters\FilterTypes\DateRangeFilter;
use App\Filters\FilterTypes\SelectFilter;
use App\Rules\UniqueStudentGroup;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use App\Traits\PerPageTrait;
use App\Traits\HasFilterTrait;
use App\Filters\FilterTypes\TextFilter;




use Illuminate\Validation\Rule;

class GroupStudent extends Model
{
    use HasFactory, PerPageTrait, HasFilterTrait;

    protected $table = 'group_students';

    protected $fillable = [
        'is_active',
        'join_date',
        'student_id',
        'group_id',
    ];

    protected $filterable = [
        'student_id' => SelectFilter::class,
        'group_id'   => SelectFilter::class,
        'join_date'   => DateRangeFilter::class,
    ];

    protected $modelRelations = [
        'group'    => Group::class,
        'student'    => Student::class,
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'group_id'      => [$required, 'exists:groups,id'],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }


    public static function studentRules(string $prefix = '', bool $is_nullable = false)
    {

        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [

            'join_date'      => ['nullable', 'date',],
            'id'      => [
                $required,
                'exists:students,id',
                new UniqueStudentGroup(request()->group_id),

            ],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
