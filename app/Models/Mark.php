<?php

namespace App\Models;

use App\Filters\FilterTypes\BooleanFilter;
use App\Filters\FilterTypes\SelectFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Mark extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'marks';

    protected $fillable = [
        'serial_number',
        'description',
        'once_a_day',
        'type',
        'max_mark',
        'min_mark',
        'is_flixable',
        'mark_package_id',
    ];

    protected $filterable = [
        'serial_number'                => TextFilter::class,
        'once_a_day'           => BooleanFilter::class,
        'type'          => TextFilter::class,
        'max_mark'     => SelectFilter::class,
        'min_mark'     => SelectFilter::class,
        'is_flixable'           => BooleanFilter::class,
        'mark_package_id' => SelectFilter::class,
    ];

    public static array $pluralRelationships = [
        'saveMark',
        'markPackage',
    ];

    public function saveMark(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(SavedMark::class, 'mark_id');
    }
    public function markPackage()
    {
        return $this->belongsTo(MarkPackage::class, 'mark_package_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
