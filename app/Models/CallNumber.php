<?php

namespace App\Models;

use App\Filters\FilterTypes\BooleanFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class CallNumber extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'call_numbers';

    protected $fillable = [
        'phone_number',
        'owner_name',
        'whatsapp',
        'call',
        'member_id',
    ];

    protected $filterable = [
        'phone_number'       => TextFilter::class,
        'owner_name'         => TextFilter::class,
        'whatsapp'           => BooleanFilter::class,
        'call'               => BooleanFilter::class,
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'phone_number' => [$required, 'regex:/^\d{10}$/'],
            'owner_name'   => [$required, 'string',],
            'whatsapp'     => [$required, 'boolean'],
            'call'         => [$required, 'boolean'],
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
