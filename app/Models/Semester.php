<?php

namespace App\Models;

use App\Filters\FilterTypes\BooleanFilter;
use App\Filters\FilterTypes\DateRangeFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Semester extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'semesters';

    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'is_active',
        // 'days',
        'time_type_id',
    ];

    protected $casts = [
        'start_date' => 'date',
        'end_date'   => 'date',
    ];


    protected $filterable = [
        'name'          => TextFilter::class,
        'start_date'    => DateRangeFilter::class,
        'end_date'      => DateRangeFilter::class,
        'is_active'     => BooleanFilter::class,
        'time_type_id'  => TextFilter::class,

    ];

    public function timeType()
    {
        return $this->belongsTo(TimeType::class);
    }

    public function myGroups()
    {
        return $this->hasMany(Group::class, 'current_semester_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
