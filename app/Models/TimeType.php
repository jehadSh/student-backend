<?php

namespace App\Models;

use App\Enums\SemesterDaysEnum;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class TimeType extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'time_types';

    protected $fillable = [
        'time',
        'days',
    ];

    protected $filterable = [
        'time'          => TextFilter::class,
        'days'          => TextFilter::class,
    ];

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'time'      => [$required,],
            'days'      => [$required, Rule::in(SemesterDaysEnum::values()),],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
