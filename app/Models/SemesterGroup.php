<?php

namespace App\Models;

use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class SemesterGroup extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'semester_groups';

    protected $fillable = [
        'group_id',
        'semester_id',
    ];


    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class, 'semester_id');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        // مطلوب الserial_number  مع الsemester_id دائمااا
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
//            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }

}
