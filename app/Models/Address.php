<?php

namespace App\Models;

use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Address extends Model
{
    use HasFactory;
    use HasFilterTrait;


    protected $table = 'addresses';

    protected $fillable = [
        'place_name',
        'specific_location',
        'member_id',
    ];
    protected $filterable = [
        'place_name'             => TextFilter::class,
        'specific_location'      => TextFilter::class,
    ];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            //            'directorate_id'      => [$required, 'exists:directorates,id',],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
