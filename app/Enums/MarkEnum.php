<?php

namespace App\Enums;


enum MarkEnum: string
{
    case ATTENDANCE = 'ATTENDANCE';
    case FROM_TEACHER = 'FROM_TEACHER10';
    case FROM_SUPERVISER = 'FROM_SUPERVISER';
    case RECITE_5_PAGES = 'RECITE_5_PAGES';
    case GOOD_BEHAVIOR = 'GOOD_BEHAVIOR';
    case PART_TEST = 'PART_TEST';
}
