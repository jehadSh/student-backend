<?php

namespace App\Enums;


enum TypeCallNumberEnum: string
{
    case CALL = 'Call';
    case WHATSAPP = 'Whatsapp';
}
