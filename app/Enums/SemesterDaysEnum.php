<?php

namespace App\Enums;


enum SemesterDaysEnum: string
{
    case summer_1 = 'Saturday , Monday , Wednesday';
    case summer_2 = 'Sunday , Tuesday , Thursday';
    case winter_1 = 'Saturday , Thursday';
    case Saturday = 'Saturday';
    case Sunday = 'Sunday,';
    case Monday = 'Monday,';
    case Tuesday = 'Tuesday,';
    case Wednesday = 'Wednesday,';
    case Thursday = 'Thursday,';
    case Friday = 'Friday,';

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
