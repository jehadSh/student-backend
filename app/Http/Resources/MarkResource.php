<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'             => $this->id,
            'serial_number'  => $this->serial_number,
            'description'    => $this->description,
            'type'           => $this->type,
            'max_mark'       => $this->max_mark,
            'min_mark'       => $this->min_mark,
            'is_flixable'    => $this->is_flixable,
            'once_a_day'     => $this->once_a_day,
            'mark_package'     => MarkPackageResource::make($this->whenLoaded('markPackage')),
        ];
    }
}
