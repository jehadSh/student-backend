<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TimeTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'   => $this->id,
            'time' => $this->time,
            'days' => $this->days,
        ];
    }
}
