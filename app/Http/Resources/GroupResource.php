<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'is_active'         => $this->is_active,
            'serial_number'     => $this->serial_number,
            'merge_group_id'    => $this->merge_group_id,
            'separate_group_id' => $this->separate_group_id,
            'teacher'           => TeacherResource::make($this->whenLoaded('teacher')),
            'school_class'      => SchoolClassResource::make($this->whenLoaded('schoolClass')),
            'students'          => StudentResource::collection($this->whenLoaded('students')),
            'current_semester'  => SemesterResource::make($this->whenLoaded('current_semester')),
        ];
    }
}
