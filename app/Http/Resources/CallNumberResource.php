<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CallNumberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'           => $this->id,
            'phone_number' => $this->phone_number,
            'owner_name'   => $this->owner_name,
            'whatsapp'     => $this->whatsapp,
            'call'         => $this->call,
            // 'tell_number'  => $this->tell_number,

        ];
    }
}
