<?php

namespace App\Http\Resources;

use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'          => $this->id,
            'first_name'  => $this->first_name,
            'last_name'   => $this->last_name,
            'birth_date'  => $this->birth_date,
            'address'     => AddressResource::collection($this->whenLoaded('address')),
            'callNumbers' => CallNumberResource::collection($this->whenLoaded('callNumbers')),
        ];
    }
}
