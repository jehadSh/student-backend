<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SemesterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'start_date'   => $this->start_date,
            'end_date'     => $this->end_date,
            'is_active'    => $this->is_active,
            // 'days'         => $this->days,
            'time_type_id' => TimeTypeResource::make($this->whenLoaded('timeType')),
            'Groups' => GroupResource::collection($this->whenLoaded('myGroups')),
        ];
    }
}
