<?php

namespace App\Http\Resources;

use App\Models\Group;
use App\Models\Semester;
use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentCheckInResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'       => $this->id,
            'is_exist' => $this->is_exist,
            'date'     => $this->date,
            'semester' => SemesterResource::make($this->whenLoaded('semester')),
            'group'    => GroupResource::make($this->whenLoaded('group')),
            'student'  => StudentResource::make($this->whenLoaded('student')),

        ];
    }
}
