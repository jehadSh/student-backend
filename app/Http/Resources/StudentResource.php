<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'            => $this->id,
            'father_name'   => $this->father_name,
            'father_work'   => $this->father_work,
            'notes'         => $this->notes,
            'is_active'     => $this->is_active,
            'member'        => MemberResource::make($this->whenLoaded('member')),
            'current_group' => GroupResource::make($this->whenLoaded('currentGroup')),
        ];
    }
}
