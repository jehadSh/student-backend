<?php

namespace App\Http\Resources;

use App\Models\Group;
use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GroupStudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'        => $this->id,
            'join_date' => $this->join_date,
            'group'     => GroupResource::make($this->whenLoaded('group')),
            'student'   => StudentResource::make($this->whenLoaded('student')),
        ];
    }
}
