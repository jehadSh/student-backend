<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SavedMarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'    => $this->id,
            'value' => $this->value,
            'date'  => $this->date,

            'student'  => StudentResource::make($this->whenLoaded('student')),
            'semester' => SemesterResource::make($this->whenLoaded('semester')),
            'mark'     => MarkResource::make($this->whenLoaded('mark')),
        ];
    }
}
