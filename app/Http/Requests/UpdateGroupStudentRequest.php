<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\GroupStudent;

class UpdateGroupStudentRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return array_merge(
            GroupStudent::rules(is_nullable: true),
            ['students' => ['required', 'array']],
            GroupStudent::studentRules('students.*.', is_nullable: true),
        );
    }
}
