<?php

namespace App\Http\Requests;

use App\Models\Address;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Teacher;

class UpdateTeacherRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return array_merge(
            Teacher::rules(is_nullable: true),
            ['address' => ['nullable', 'array']],
            Address::rules('address.*.', is_nullable: true),
        );
    }
}
