<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\GroupStudent;

class CreateGroupStudentRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return array_merge(
            GroupStudent::rules(),
            ['students' => ['required', 'array']],
            GroupStudent::studentRules('students.*.'),
        );
    }
}
