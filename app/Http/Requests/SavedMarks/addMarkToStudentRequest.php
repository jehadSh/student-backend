<?php

namespace App\Http\Requests\SavedMarks;

use Illuminate\Foundation\Http\FormRequest;

class addMarkToStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'student_id' => ['required', 'exists:students,id'],
            'mark_id' => ['required', 'exists:marks,id'],
            'semester_id' => ['required', 'exists:semesters,id'],
            'value' => ['nullable', 'integer'],
        ];
    }
}
