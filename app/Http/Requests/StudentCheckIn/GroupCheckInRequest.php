<?php

namespace App\Http\Requests\StudentCheckIn;

use Illuminate\Foundation\Http\FormRequest;

class GroupCheckInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'date' => ['required', 'date'],
            'is_exist' => ['required', 'boolean'],
            'students_id' => ['required', 'array'],
            'students_id.*' => ['required', 'exists:students,id'],
            'semester_id' => ['required', 'exists:semesters,id'],

            // 'group_id' => ['required', 'exists:groups,id'],
        ];
    }
}
