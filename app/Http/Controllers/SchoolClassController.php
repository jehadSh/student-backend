<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSchoolClassRequest;
use App\Http\Requests\UpdateSchoolClassRequest;
use App\Http\Resources\SchoolClassResource;
use App\Models\SchoolClass;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\SchoolClassService;

class SchoolClassController extends Controller
{
    public function __construct(protected SchoolClassService $schoolClassService)
    {
    }

    public function all()
    {
        $SchoolClasss = $this->schoolClassService->all();
        return ApiResponseService::successResponse(SchoolClassResource::collection($SchoolClasss));
    }

    public function index()
    {
        $SchoolClasss = $this->schoolClassService->index();
        return ApiResponseService::successResponse(SchoolClassResource::collection($SchoolClasss));
    }

    public function store(CreateSchoolClassRequest $request)
    {
        $SchoolClasss = $this->schoolClassService->store($request);
        return ApiResponseService::createdResponse(data: SchoolClassResource::make($SchoolClasss));
    }

    public function show(SchoolClass $schoolClass)
    {
        $SchoolClasss = $this->schoolClassService->show($schoolClass);
        return ApiResponseService::successResponse(SchoolClassResource::make($SchoolClasss));
    }

    public function update(UpdateSchoolClassRequest $request, SchoolClass $schoolClass)
    {
        $SchoolClasss = $this->schoolClassService->update($request, $schoolClass);
        return ApiResponseService::successResponse(SchoolClassResource::make($SchoolClasss));
    }

    public function destroy(SchoolClass $schoolClass)
    {
        $this->schoolClassService->destroy($schoolClass);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

