<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMarkRequest;
use App\Http\Requests\UpdateMarkRequest;
use App\Http\Resources\MarkResource;
use App\Models\Mark;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\MarkService;

class MarkController extends Controller
{
    public function __construct(protected MarkService $markService) {}

    public function all()
    {
        $Marks = $this->markService->all();
        return ApiResponseService::successResponse(MarkResource::collection($Marks));
    }

    public function index()
    {
        $Marks = $this->markService->index();
        return ApiResponseService::successResponse(MarkResource::collection($Marks));
    }

    public function store(CreateMarkRequest $request)
    {
        $Marks = $this->markService->store($request);
        $Marks->load(Mark::$pluralRelationships);
        return ApiResponseService::createdResponse(data: MarkResource::make($Marks));
    }

    public function show(Mark $mark)
    {
        $Marks = $this->markService->show($mark);
        $Marks->load(Mark::$pluralRelationships);
        return ApiResponseService::successResponse(MarkResource::make($Marks));
    }

    public function update(UpdateMarkRequest $request, Mark $mark)
    {
        $Marks = $this->markService->update($request, $mark);
        $Marks->load(Mark::$pluralRelationships);
        return ApiResponseService::successResponse(MarkResource::make($Marks));
    }

    public function destroy(Mark $mark)
    {
        $this->markService->destroy($mark);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}
