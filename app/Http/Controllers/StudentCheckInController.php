<?php

namespace App\Http\Controllers;

use App\Models\StudentCheckIn;
use App\Http\Controllers\Controller;
use App\Services\ApiResponseService;
use App\Http\Resources\StudentCheckInResource;
use App\Http\Requests\CreateStudentCheckInRequest;
use App\Http\Requests\UpdateStudentCheckInRequest;
use App\Services\ModelsServices\StudentCheckInService;
use App\Http\Requests\StudentCheckIn\GroupCheckInRequest;
use App\Models\Student;
use Carbon\Carbon;
use PhpParser\Node\Stmt\Continue_;

class StudentCheckInController extends Controller
{
    public function __construct(protected StudentCheckInService $studentCheckInService) {}

    public function all()
    {
        $StudentCheckIns = $this->studentCheckInService->all();
        return ApiResponseService::successResponse(StudentCheckInResource::collection($StudentCheckIns));
    }

    public function index()
    {
        $StudentCheckIns = $this->studentCheckInService->index();
        return ApiResponseService::successResponse(StudentCheckInResource::collection($StudentCheckIns));
    }

    public function store(CreateStudentCheckInRequest $request)
    {
        $date = $request->date;
        $student = Student::find($request->student_id);
        $days = $student->currentGroup->current_semester->timeType->days;
        $today = Carbon::parse($date)->dayName;
        if (!str_contains($days, $today)) {
            return ApiResponseService::errorResponse("The Date $date is $today and it is not one of the semester days ($days).");
        }

        $StudentCheckIns = $this->studentCheckInService->store($request);
        return ApiResponseService::createdResponse(data: StudentCheckInResource::make($StudentCheckIns));
    }

    public function show(StudentCheckIn $studentCheckIn)
    {
        $StudentCheckIns = $this->studentCheckInService->show($studentCheckIn);
        $StudentCheckIns->load('semester', 'group', 'student.member');

        return ApiResponseService::successResponse(StudentCheckInResource::make($StudentCheckIns));
    }

    public function update(UpdateStudentCheckInRequest $request, StudentCheckIn $studentCheckIn)
    {
        $StudentCheckIns = $this->studentCheckInService->update($request, $studentCheckIn);
        $StudentCheckIns->load('semester', 'group', 'student.member');
        return ApiResponseService::successResponse(StudentCheckInResource::make($StudentCheckIns));
    }

    public function destroy(StudentCheckIn $studentCheckIn)
    {
        $this->studentCheckInService->destroy($studentCheckIn);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }

    public function GroupCheckIn(GroupCheckInRequest $request)
    {
        $data = $request->validated();
        $processedStudents = [];
        $studentsWithError = [];

        foreach ($data['students_id'] as $student_id) {
            $student = Student::find($student_id);

            if (!$student->currentGroup()) {
                $studentsWithError[] = $student->id;
                continue;
            }

            $date = $data['date'];
            $is_exist = $data['is_exist'];
            $group_id = $student->currentGroup()->id;
            $semester_id = $data['semester_id'];

            StudentCheckIn::create([
                'date' => $date,
                'is_exist' => $is_exist,
                'student_id' => $student->id,
                'group_id' => $group_id,
                'semester_id' => $semester_id,
            ]);

            $processedStudents[]
                = StudentCheckIn::create([
                    'date' => $date,
                    'is_exist' => $is_exist,
                    'student_id' => $student->id,
                    'group_id' => $group_id,
                    'semester_id' => $semester_id,
                ])->load('semester', 'group', 'student');
        }
        return ApiResponseService::successResponse(StudentCheckInResource::collection($processedStudents));
    }
}
