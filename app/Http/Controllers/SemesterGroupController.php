<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSemesterGroupRequest;
use App\Http\Requests\UpdateSemesterGroupRequest;
use App\Http\Resources\SemesterGroupResource;
use App\Models\SemesterGroup;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\SemesterGroupService;

class SemesterGroupController extends Controller
{
    public function __construct(protected SemesterGroupService $SemesterGroupService)
    {
    }

    public function all()
    {
        $SemesterGroups = $this->SemesterGroupService->all();
        return ApiResponseService::successResponse(SemesterGroupResource::collection($SemesterGroups));
    }

    public function index()
    {
        $SemesterGroups = $this->SemesterGroupService->index();
        return ApiResponseService::successResponse(SemesterGroupResource::collection($SemesterGroups));
    }

    public function store(CreateSemesterGroupRequest $request)
    {
        $SemesterGroups = $this->SemesterGroupService->store($request);
        return ApiResponseService::createdResponse(data: SemesterGroupResource::make($SemesterGroups));
    }

    public function show(SemesterGroup $SemesterGroup_var)
    {
        $SemesterGroups = $this->SemesterGroupService->show($SemesterGroup_var);
        return ApiResponseService::successResponse(SemesterGroupResource::make($SemesterGroups));
    }

    public function update(UpdateSemesterGroupRequest $request, SemesterGroup $SemesterGroup_var)
    {
        $SemesterGroups = $this->SemesterGroupService->update($request, $SemesterGroup_var);
        return ApiResponseService::successResponse(SemesterGroupResource::make($SemesterGroups));
    }

    public function destroy(SemesterGroup $SemesterGroup_var)
    {
        $this->SemesterGroupService->destroy($SemesterGroup_var);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

