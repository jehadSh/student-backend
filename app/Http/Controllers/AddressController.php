<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\AddressService;

class AddressController extends Controller
{
    public function __construct(protected AddressService $AddressService)
    {
    }

    public function all()
    {
        $Addresss = $this->AddressService->all();
        return ApiResponseService::successResponse(AddressResource::collection($Addresss));
    }

    public function index()
    {
        $Addresss = $this->AddressService->index();
        return ApiResponseService::successResponse(AddressResource::collection($Addresss));
    }

    public function store(CreateAddressRequest $request)
    {
        $Addresss = $this->AddressService->store($request);
        return ApiResponseService::createdResponse(data: AddressResource::make($Addresss));
    }

    public function show(Address $Address_var)
    {
        $Addresss = $this->AddressService->show($Address_var);
        return ApiResponseService::successResponse(AddressResource::make($Addresss));
    }

    public function update(UpdateAddressRequest $request, Address $Address_var)
    {
        $Addresss = $this->AddressService->update($request, $Address_var);
        return ApiResponseService::successResponse(AddressResource::make($Addresss));
    }

    public function destroy(Address $Address_var)
    {
        $this->AddressService->destroy($Address_var);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
    public function test (){
        return "test";
    }
}

