<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEducationLevelRequest;
use App\Http\Requests\UpdateEducationLevelRequest;
use App\Http\Resources\EducationLevelResource;
use App\Models\EducationLevel;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\EducationLevelService;

class EducationLevelController extends Controller
{
    public function __construct(protected EducationLevelService $EducationLevelService)
    {
    }

    public function all()
    {
        $EducationLevels = $this->EducationLevelService->all();
        return ApiResponseService::successResponse(EducationLevelResource::collection($EducationLevels));
    }

    public function index()
    {
        $EducationLevels = $this->EducationLevelService->index();
        return ApiResponseService::successResponse(EducationLevelResource::collection($EducationLevels));
    }

    public function store(CreateEducationLevelRequest $request)
    {
        $EducationLevels = $this->EducationLevelService->store($request);
        return ApiResponseService::createdResponse(data: EducationLevelResource::make($EducationLevels));
    }

    public function show(EducationLevel $educationLevel)
    {
        $EducationLevels = $this->EducationLevelService->show($educationLevel);
        return ApiResponseService::successResponse(EducationLevelResource::make($EducationLevels));
    }

    public function update(UpdateEducationLevelRequest $request, EducationLevel $educationLevel)
    {
        $EducationLevels = $this->EducationLevelService->update($request, $educationLevel);
        return ApiResponseService::successResponse(EducationLevelResource::make($EducationLevels));
    }

    public function destroy(EducationLevel $educationLevel)
    {
        $this->EducationLevelService->destroy($educationLevel);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

