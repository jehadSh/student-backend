<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateGroupStudentRequest;
use App\Http\Requests\UpdateGroupStudentRequest;
use App\Http\Resources\GroupStudentResource;
use App\Models\GroupStudent;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\GroupStudentService;

class GroupStudentController extends Controller
{
    public function __construct(protected GroupStudentService $GroupStudentService)
    {
    }

    public function all()
    {
        $GroupStudents = $this->GroupStudentService->all();
        return ApiResponseService::successResponse(GroupStudentResource::collection($GroupStudents));
    }

    public function index()
    {
        $GroupStudents = $this->GroupStudentService->index();
        return ApiResponseService::successResponse(GroupStudentResource::collection($GroupStudents));
    }

    public function createMany(CreateGroupStudentRequest $request)
    {
        $groupStudent = $this->GroupStudentService->createMany($request);
        // return $groupStudent;
        return ApiResponseService::createdResponse(data: GroupStudentResource::collection($groupStudent));
    }

    public function show(GroupStudent $groupStudent)
    {
        $groupStudent = $this->GroupStudentService->show($groupStudent);
        return ApiResponseService::successResponse(GroupStudentResource::make($groupStudent));
    }

    public function update(UpdateGroupStudentRequest $request)
    {
        $groupStudent = $this->GroupStudentService->updateMany($request);
        return ApiResponseService::successResponse(GroupStudentResource::collection($groupStudent));
    }

    public function destroy(GroupStudent $groupStudent)
    {
        $this->GroupStudentService->destroy($groupStudent);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}
