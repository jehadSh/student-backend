<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Http\Resources\GroupResource;
use App\Models\Group;
use App\Models\Student;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\GroupService;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function __construct(protected GroupService $groupService)
    {
    }

    public function all()
    {
        $groups = $this->groupService->all();
        return ApiResponseService::successResponse(GroupResource::collection($groups));
    }

    public function all_with_trashed()
    {
        $groups = $this->groupService->all_with_trashed();
        return ApiResponseService::successResponse(GroupResource::collection($groups));
    }

    public function index()
    {
        $groups = $this->groupService->index();
        return ApiResponseService::successResponse(GroupResource::collection($groups));
    }

    public function store(CreateGroupRequest $request)
    {
        $groups = $this->groupService->store($request);
        $groups->loadMissing('schoolClass', 'teacher', 'teacher.member', 'current_semester');
        return ApiResponseService::createdResponse(data: GroupResource::make($groups));
    }

    public function show(Group $group)
    {
        $groups = $this->groupService->show($group);
        $groups->loadMissing('schoolClass', 'teacher', 'teacher.member', 'current_semester');
        return ApiResponseService::successResponse(GroupResource::make($groups));
    }

    public function update(UpdateGroupRequest $request, Group $group)
    {
        $groups = $this->groupService->update($request, $group);
        $groups->loadMissing('schoolClass', 'teacher', 'teacher.member', 'current_semester');
        return ApiResponseService::successResponse(GroupResource::make($groups));
    }

    public function destroy(Group $group)
    {
        $this->groupService->destroy($group);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }

    public function AssignStudentToGroup(Request $request, Group $group)
    {
        $groups = $this->groupService->AssignStudentToGroup($request, $group);
        $groups->loadMissing('students', 'students.member');
        return ApiResponseService::successResponse(GroupResource::make($groups));
    }

    public function giveGroupReward(Request $request, Group $group)
    {
        $this->groupService->giveGroupReward($request, $group);
        return ApiResponseService::successResponse(null, 'done');
    }
}

