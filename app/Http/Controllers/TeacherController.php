<?php

namespace App\Http\Controllers;

use App\DTOs\TeacherDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTeacherRequest;
use App\Http\Requests\UpdateTeacherRequest;
use App\Http\Resources\TeacherResource;
use App\Models\Teacher;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\TeacherService;

class TeacherController extends Controller
{
    public function __construct(protected TeacherService $teacherService)
    {
    }

    public function all()
    {
        $teacher = $this->teacherService->all();
        return ApiResponseService::successResponse(TeacherResource::collection($teacher));
    }

    public function index()
    {
        $teacher = $this->teacherService->index();
        return ApiResponseService::successResponse(TeacherResource::collection($teacher));
    }

    public function store(CreateTeacherRequest $request)
    {
        $teacher = $this->teacherService->store($request);
        $teacher->loadMissing('member', 'member.address', 'member.callNumbers');
        return ApiResponseService::createdResponse(data: TeacherResource::make($teacher));
    }

    public function show(Teacher $teacher)
    {
        $teacher = $this->teacherService->show($teacher);
        $teacher->loadMissing('member', 'member.address', 'member.callNumbers');
        return ApiResponseService::successResponse(TeacherResource::make($teacher));
    }

    public function update(UpdateTeacherRequest $request, Teacher $teacher)
    {
          $teacher = $this->teacherService->updateAll($request, $teacher);
        $teacher->loadMissing('member', 'member.address', 'member.callNumbers');
        return ApiResponseService::successResponse(TeacherResource::make($teacher));
    }

    public function destroy(Teacher $teacher)
    {
        $this->teacherService->destroy($teacher);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}
