<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Resources\StudentResource;
use App\Models\Group;
use App\Models\Student;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\StudentService;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct(protected StudentService $studentervice) {}

    public function all()
    {
        $student = $this->studentervice->all();
        return ApiResponseService::successResponse(StudentResource::collection($student));
    }
    public function allWithInActive()
    {
        $student = $this->studentervice->allWithInActive();
        return ApiResponseService::successResponse(StudentResource::collection($student));
    }

    public function index()
    {
        $student = $this->studentervice->index();
        return ApiResponseService::successResponse(StudentResource::collection($student));
    }

    public function store(CreateStudentRequest $request)
    {
        $student = $this->studentervice->store($request);
        $student->loadMissing('member', 'member.address', 'member.callNumbers', 'currentGroup.schoolClass');
        return ApiResponseService::createdResponse(data: StudentResource::make($student));
    }

    public function show(Student $student)
    {
        $student = $this->studentervice->show($student);
        $student->loadMissing('member', 'member.address', 'member.callNumbers', 'currentGroup.schoolClass', 'currentGroup.current_semester');
        return ApiResponseService::successResponse(StudentResource::make($student));
    }

    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student = $this->studentervice->updateAll($request, $student);
        $student->loadMissing('member', 'member.address', 'member.callNumbers', 'currentGroup');
        return ApiResponseService::successResponse(StudentResource::make($student));
    }
    public function activeStudent(Group $group, Request $request)
    {
        $this->studentervice->activeStudent($group, $request);
        return ApiResponseService::successResponse('done');
    }

    public function destroy(Student $student)
    {
        if ($student->marks()->count() > 0) {
            return ApiResponseService::errorResponse("Cannot delete the student, because the student have marks.");
        }
        $this->studentervice->destroy($student);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }

    public function force_destroy(Student $student)
    {
        $this->studentervice->destroy($student);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}
