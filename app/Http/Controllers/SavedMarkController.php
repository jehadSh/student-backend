<?php

namespace App\Http\Controllers;

use App\Models\SavedMark;
use App\Services\ApiResponseService;
use App\Http\Resources\SavedMarkResource;
use App\Http\Requests\CreateSavedMarkRequest;
use App\Http\Requests\UpdateSavedMarkRequest;
use App\Services\ModelsServices\SavedMarkService;
use App\Http\Requests\SavedMarks\addMarkToStudentRequest;
use Carbon\Carbon;

class SavedMarkController extends Controller
{
    public function __construct(protected SavedMarkService $savedMarkService) {}

    public function all()
    {
        $SavedMarks = $this->savedMarkService->all();
        return ApiResponseService::successResponse(SavedMarkResource::collection($SavedMarks));
    }

    public function index()
    {
        $SavedMarks = $this->savedMarkService->index();
        return ApiResponseService::successResponse(SavedMarkResource::collection($SavedMarks));
    }

    public function store(CreateSavedMarkRequest $request)
    {
        $old = SavedMark::where('student_id', $request->student_id)
            ->where('mark_id', $request->mark_id)
            ->where('date', $request->date)
            ->with('mark')
            ->get();
        if ($old->count() != 0 && $old[0]->mark->once_a_day) {
            return ApiResponseService::errorResponse('This student have this mark at ' . $request->date . '.');
        }
        $SavedMarks = $this->savedMarkService->store($request);
        return ApiResponseService::createdResponse(data: SavedMarkResource::make($SavedMarks));
    }

    public function show(SavedMark $saveMark)
    {
        $SavedMarks = $this->savedMarkService->show($saveMark);
        $SavedMarks->load('student', 'semester', 'mark');
        return ApiResponseService::successResponse(SavedMarkResource::make($SavedMarks));
    }

    public function update(UpdateSavedMarkRequest $request, SavedMark $saveMark)
    {
        $SavedMarks = $this->savedMarkService->update($request, $saveMark);
        return ApiResponseService::successResponse(SavedMarkResource::make($SavedMarks));
    }

    public function destroy(SavedMark $saveMark)
    {
        $this->savedMarkService->destroy($saveMark);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }

    public function addMarkToStudent(addMarkToStudentRequest $request)
    {
        $data = $request->validated();
        return ApiResponseService::successResponse(SavedMark::make($data));
    }
}
