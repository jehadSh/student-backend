<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCallNumberRequest;
use App\Http\Requests\UpdateCallNumberRequest;
use App\Http\Resources\CallNumberResource;
use App\Models\CallNumber;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\CallNumberService;

class CallNumberController extends Controller
{
    public function __construct(protected CallNumberService $CallNumberService)
    {
    }

    public function all()
    {
        $CallNumbers = $this->CallNumberService->all();
        return ApiResponseService::successResponse(CallNumberResource::collection($CallNumbers));
    }

    public function index()
    {
        $CallNumbers = $this->CallNumberService->index();
        return ApiResponseService::successResponse(CallNumberResource::collection($CallNumbers));
    }

    public function store(CreateCallNumberRequest $request)
    {
        $CallNumbers = $this->CallNumberService->store($request);
        return ApiResponseService::createdResponse(data: CallNumberResource::make($CallNumbers));
    }

    public function show(CallNumber $CallNumber_var)
    {
        $CallNumbers = $this->CallNumberService->show($CallNumber_var);
        return ApiResponseService::successResponse(CallNumberResource::make($CallNumbers));
    }

    public function update(UpdateCallNumberRequest $request, CallNumber $CallNumber_var)
    {
        $CallNumbers = $this->CallNumberService->update($request, $CallNumber_var);
        return ApiResponseService::successResponse(CallNumberResource::make($CallNumbers));
    }

    public function destroy(CallNumber $CallNumber_var)
    {
        $this->CallNumberService->destroy($CallNumber_var);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

