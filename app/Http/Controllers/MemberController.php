<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMemberRequest;
use App\Http\Requests\UpdateMemberRequest;
use App\Http\Resources\MemberResource;
use App\Models\Member;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\MemberService;
use Illuminate\Http\Request;

class MemberController extends Controller
{

    public function __construct(protected MemberService $memberService) {}

    public function all()
    {
        $memberes = $this->memberService->all();
        return ApiResponseService::successResponse(MemberResource::collection($memberes));
    }

    public function index()
    {
        $memberes = $this->memberService->index();
        return ApiResponseService::successResponse(MemberResource::collection($memberes));
    }

    public function store(CreateMemberRequest $request)
    {
        $member = $this->memberService->store($request);
        return ApiResponseService::createdResponse(data: MemberResource::make($member));
    }

    public function show(Member $member)
    {
        $member = $this->memberService->show($member);
        $member->load(Member::$pluralRelationships);
        return ApiResponseService::successResponse(MemberResource::make($member));
    }

    public function update(UpdateMemberRequest $request, Member $member)
    {
        $member = $this->memberService->update($request, $member);
        return ApiResponseService::successResponse(MemberResource::make($member));
    }

    public function destroy(Member $member)
    {
        $this->memberService->destroy($member);
        return ApiResponseService::successResponse(null, 'Deleted Successfuly');
    }
}
