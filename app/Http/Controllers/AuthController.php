<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        $user = User::create($request->all());
        $user->update([
            'password' => bcrypt($request->password),
        ]);
        $token = $user->createToken('my-token')->plainTextToken;
        $response = ['user' => $user, 'token' => $token];
        return response()->json($response, 201);
    }

    public function login(Request $request)
    {
        if ($request->email == null || $request->password == null) {
            return response()->json('please enter your email or your password', 201);
        }
        $fields = $request->validate([
            'email' => 'required',
            'password' => 'required|string'
        ]);
        // Check phone
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad creds'
            ], 401);
        }

        $token = $user->createToken('my-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response()->json($response, 201);
    }


    public function logout(Request $request)
    {
        Auth::user()->tokens()->delete();
        return response()->json('Logged out', 200);
    }
}
