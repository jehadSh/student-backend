<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSemesterRequest;
use App\Http\Requests\UpdateSemesterRequest;
use App\Http\Resources\SemesterResource;
use App\Models\Semester;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\SemesterService;

class SemesterController extends Controller
{
    public function __construct(protected SemesterService $SemesterService)
    {
    }

    public function all()
    {
        $Semesters = $this->SemesterService->all();
        return ApiResponseService::successResponse(SemesterResource::collection($Semesters));
    }

    public function index()
    {
        $Semesters = $this->SemesterService->index();
        return ApiResponseService::successResponse(SemesterResource::collection($Semesters));
    }

    public function store(CreateSemesterRequest $request)
    {
        $Semesters = $this->SemesterService->store($request);
        $Semesters->loadMissing('timeType', 'myGroups');
        return ApiResponseService::createdResponse(data: SemesterResource::make($Semesters));
    }

    public function show(Semester $semester)
    {
        $Semesters = $this->SemesterService->show($semester);
        $Semesters->loadMissing('timeType', 'myGroups');
        return ApiResponseService::successResponse(SemesterResource::make($Semesters));
    }

    public function update(UpdateSemesterRequest $request, Semester $semester)
    {
        $Semesters = $this->SemesterService->update($request, $semester);
        $Semesters->loadMissing('timeType', 'myGroups');
        return ApiResponseService::successResponse(SemesterResource::make($Semesters));
    }

    public function destroy(Semester $semester)
    {
        $this->SemesterService->destroy($semester);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

