<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTimeTypeRequest;
use App\Http\Requests\UpdateTimeTypeRequest;
use App\Http\Resources\TimeTypeResource;
use App\Models\TimeType;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\TimeTypeService;

class TimeTypeController extends Controller
{
    public function __construct(protected TimeTypeService $timeTypeService)
    {
    }

    public function all()
    {
        $TimeTypes = $this->timeTypeService->all();
        return ApiResponseService::successResponse(TimeTypeResource::collection($TimeTypes));
    }

    public function index()
    {
        $TimeTypes = $this->timeTypeService->index();
        return ApiResponseService::successResponse(TimeTypeResource::collection($TimeTypes));
    }

    public function store(CreateTimeTypeRequest $request)
    {
        $TimeTypes = $this->timeTypeService->store($request);
        return ApiResponseService::createdResponse(data: TimeTypeResource::make($TimeTypes));
    }

    public function show(TimeType $timeType)
    {
        $TimeTypes = $this->timeTypeService->show($timeType);
        return ApiResponseService::successResponse(TimeTypeResource::make($TimeTypes));
    }

    public function update(UpdateTimeTypeRequest $request, TimeType $timeType)
    {
        $TimeTypes = $this->timeTypeService->update($request, $timeType);
        return ApiResponseService::successResponse(TimeTypeResource::make($TimeTypes));
    }

    public function destroy(TimeType $timeType)
    {
        $this->timeTypeService->destroy($timeType);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

