<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\User\CreateUserRequest;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return response($users);
    }

    public function create(CreateUserRequest $request)
    {
        $data = $request->validated();

        $user = User::create($data);

        return response($user, 201);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return response($user);
    }

    public function update(CreateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $data = $request->validated();

        $user->update($data);

        return response($user);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(['message' => 'User deleted successfully']);
    }
}
