<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMarkPackageRequest;
use App\Http\Requests\UpdateMarkPackageRequest;
use App\Http\Resources\MarkPackageResource;
use App\Models\MarkPackage;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\MarkPackageService;

class MarkPackageController extends Controller
{
    public function __construct(protected MarkPackageService $MarkPackageService)
    {
    }

    public function all()
    {
        $MarkPackages = $this->MarkPackageService->all();
        return ApiResponseService::successResponse(MarkPackageResource::collection($MarkPackages));
    }

    public function index()
    {
        $MarkPackages = $this->MarkPackageService->index();
        return ApiResponseService::successResponse(MarkPackageResource::collection($MarkPackages));
    }

    public function store(CreateMarkPackageRequest $request)
    {
        $MarkPackages = $this->MarkPackageService->store($request);
        return ApiResponseService::createdResponse(data: MarkPackageResource::make($MarkPackages));
    }

    public function show(MarkPackage $MarkPackage_var)
    {
        $MarkPackages = $this->MarkPackageService->show($MarkPackage_var);
        return ApiResponseService::successResponse(MarkPackageResource::make($MarkPackages));
    }

    public function update(UpdateMarkPackageRequest $request, MarkPackage $MarkPackage_var)
    {
        $MarkPackages = $this->MarkPackageService->update($request, $MarkPackage_var);
        return ApiResponseService::successResponse(MarkPackageResource::make($MarkPackages));
    }

    public function destroy(MarkPackage $MarkPackage_var)
    {
        $this->MarkPackageService->destroy($MarkPackage_var);
        return ApiResponseService::successResponse(null, 'Deleted Successfully');
    }
}

