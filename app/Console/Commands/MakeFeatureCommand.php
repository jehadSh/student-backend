<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeFeatureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:feature {name}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new feature with all related classes and files';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->argument('name');

        // Paths to your stubs
        $stubsPath = resource_path('stubs/feature');

        // Define mappings from stub to destination path
        $filesToCreate = [
            "$stubsPath/controller.stub"    => app_path("Http/Controllers/{$name}Controller.php"),
            "$stubsPath/createRequest.stub" => app_path("Http/Requests/Create{$name}Request.php"),
            "$stubsPath/updateRequest.stub" => app_path("Http/Requests/Update{$name}Request.php"),
            "$stubsPath/dto.stub"           => app_path("DTOs/{$name}DTO.php"),
            "$stubsPath/model.stub"         => app_path("Models/{$name}.php"),
            "$stubsPath/resource.stub"      => app_path("Http/Resources/{$name}Resource.php"),
            "$stubsPath/service.stub"       => app_path("Services/ModelsServices/{$name}Service.php"),
        ];

        foreach ($filesToCreate as $stubPath => $destPath) {
            $this->createFileFromStub($stubPath, $destPath, $name);
        }

        $this->info("Feature {$name} created successfully.");
    }

    protected function createFileFromStub($stubPath, $destPath, $name)
    {
        if (file_exists($destPath)) {
            $this->error("File already exists: {$destPath}");
            return;
        }

        $content = file_get_contents($stubPath);
        $content = str_replace('{{className}}', $name, $content);

        file_put_contents($destPath, $content);
    }

}
