<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class StudentCheckInDTO extends ObjectData
{
    public ?string $date;
    public ?string $is_exist;
    public ?string $student_id;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'date'        => $request->date,
            'is_exist'    => $request->is_exist,
            'student_id'  => $request->student_id,
        ]);
    }
}
