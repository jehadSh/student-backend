<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Spatie\DataTransferObject\DataTransferObject;
use Illuminate\Database\Eloquent\Model;
use ReflectionProperty;
use ReflectionClass;




abstract class ObjectData extends DataTransferObject
{
    abstract static function fromRequest(Request|FormRequest $request);

    public function mergeForUpdate(Model $model): ObjectData|static
    {
        $DTO = $this;
        foreach ($DTO->all() as $key => $value) {
            if ($DTO->$key == null) {
                $DTO->$key = $model->$key;
            }
        }
        return $DTO;
    }
    protected static function oneFromRequest($request, $field)
    {
        $request = (object) $request;
        if ($request instanceof FormRequest) {
            return $request->has($field) ? $request->$field : UnsetValue::instance();
        } else {
            return property_exists($request, $field) ? $request->$field : UnsetValue::instance();
        }
    }

    protected static function fromRequestHelper($request)
    {
        $reflection = new ReflectionClass(static::class);
        $properties = $reflection->getProperties(
            ReflectionProperty::IS_PUBLIC
            // |   ReflectionProperty::IS_PROTECTED
            // |   ReflectionProperty::IS_PRIVATE
        );
        $result = [];
        foreach ($properties as $property) {
            if (!$property->isStatic()) {
                $propName = $property->getName();
                $result[$propName] = self::oneFromRequest($request, $propName);
            }
        }
        return $result;
    }
}

class UnsetValue {
    private static $instance;
    private function __construct() {
    }
    public static function instance() {
        if(self::$instance==null){
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function __toString()
    {
        return "";
    }
}

