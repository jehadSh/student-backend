<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class GroupStudentDTO extends ObjectData
{
    public ?string $join_date;
    public ?string $student_id;
    public ?string $group_id;

    public static function fromRequest($request): self
    {
        return new self([
            'join_date' => $request['join_date'],
            'student_id' => $request['student_id'],
            'group_id' => $request['group_id'],

        ]);
    }
}
