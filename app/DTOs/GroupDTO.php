<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class GroupDTO extends ObjectData
{
    public ?string $name;
    public ?string $is_active;
    public ?string $serial_number;
    public ?string $teacher_id;
    public ?string $merge_group_id;
    public ?string $separate_group_id;
    public ?string $school_class_id;
    public ?string $current_semester_id;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'name'                => $request->name,
            'is_active'           => $request->is_active,
            'serial_number'       => $request->serial_number,
            'teacher_id'          => $request->teacher_id,
            'merge_group_id'      => $request->merge_group_id,
            'separate_group_id'   => $request->separate_group_id,
            'school_class_id'     => $request->school_class_id,
            'current_semester_id' => $request->current_semester_id,
        ]);
    }
}
