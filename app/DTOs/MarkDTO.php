<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class MarkDTO extends ObjectData
{
    public ?string $serial_number;
    public ?string $description;
    public ?string $type;
    public ?string $max_mark;
    public ?string $min_mark;
    public ?string $once_a_day;
    public ?string $is_flixable;
    public ?string $mark_package_id;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'serial_number'   => $request->serial_number,
            'description'     => $request->description,
            'once_a_day'      => $request->once_a_day,
            'type'            => $request->type,
            'max_mark'        => $request->max_mark,
            'min_mark'        => $request->min_mark,
            'is_flixable'     => $request->is_flixable,
            'mark_package_id' => $request->mark_package_id,
        ]);
    }
}
