<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class MemberDTO extends ObjectData
{
    public static ?int $id = null;
    public ?string $first_name;
    public ?string $last_name;
    public ?string $birth_date;

    public static function fromRequest(Request|FormRequest $request)
    {
        return new self(
            [
                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'birth_date' => $request->birth_date,
            ]
        );
    }

}
