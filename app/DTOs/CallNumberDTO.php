<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class CallNumberDTO extends ObjectData
{
    public ?string $phone_number;
    public ?string $owner_name;
    public ?string $whatsapp;
    public ?string $call;

    // public ?string $tell_number;

    public static function fromRequest($request): self
    {
        return new self([
            'phone_number' => $request['phone_number'],
            'owner_name'   => $request['owner_name'],
            'whatsapp'     => $request['whatsapp'],
            'call'         => $request['call'],

        ]);
    }

    public static function fromUpdateRequest($request): self
    {
        return new self([
            'phone_number' => $request->phone_number,
            'owner_name'   => $request->owner_name,
            'whatsapp'     => $request->whatsapp,
            'call'         => $request->call,
        ]);
    }
}
