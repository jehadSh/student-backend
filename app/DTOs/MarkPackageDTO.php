<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class MarkPackageDTO extends ObjectData
{
    public ?string $name;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'name' => $request->name,
        ]);
    }
}

