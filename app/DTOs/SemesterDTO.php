<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class SemesterDTO extends ObjectData
{
    public ?string $name;
    public ?string $start_date;
    public ?string $end_date;
    public ?string $is_active;
    // public ?string $days;
    public ?string $time_type_id;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'name'         => $request->name,
            'start_date'   => $request->start_date,
            'end_date'     => $request->end_date,
            'is_active'    => $request->is_active,
            // 'days'         => $request->days,
            'time_type_id' => $request->time_type_id,
        ]);
    }
}
