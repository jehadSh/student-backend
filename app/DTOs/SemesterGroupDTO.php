<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class SemesterGroupDTO extends ObjectData
{
    public ?string $group_id;
    public ?string $semester_id;

    public static function fromRequest($request): self
    {
        return new self([
            'group_id'    => $request['group_id'],
            'semester_id' => $request['semester_id'],
        ]);
    }
}

