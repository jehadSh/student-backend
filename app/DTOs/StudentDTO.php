<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class StudentDTO extends ObjectData
{
    public ?string $father_name;
    public ?string $father_work;
    public ?string $notes;
    public ?string $current_group_id;
    public ?string $is_active;

    public static function fromRequest($request): self
    {

        return new self([
            'father_name'      => $request['father_name'],
            'father_work'      => $request['father_work'],
            'notes'            => $request['notes'] ?? null,
            'current_group_id' => $request['current_group_id'],
            'is_active'        => $request['is_active'] ?? 1,
        ]);
    }
}
