<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class TimeTypeDTO extends ObjectData
{
    public ?string $time;
    public ?string $days;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'time' => $request->time,
            'days' => $request->days,
        ]);
    }
}
