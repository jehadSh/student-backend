<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class TeacherDTO extends ObjectData
{
    public ?string $type;
    // public ?string $education_level_id;

    public static function fromRequest($request): self
    {
        return new self([
            'type'               => $request['type'],
            // 'education_level_id' => $request['education_level_id'],
        ]);
    }

    public static function manyFromRequest(array $elements)
    {
        $objects = [];
        foreach ($elements as $element) {
            $object = new self(
                self::fromRequestHelper($element)
            );
            $objects[] = $object->all();
        }
        return $objects;
    }
}
