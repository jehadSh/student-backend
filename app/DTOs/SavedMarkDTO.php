<?php

namespace App\DTOs;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class SavedMarkDTO extends ObjectData
{
    public ?string $value;
    public ?string $student_id;
    public ?string $semester_id;
    public ?string $mark_id;
    public ?string $date;

    public static function fromRequest(Request|FormRequest $request): self
    {
        return new self([
            'value'       => $request->value,
            'student_id'  => $request->student_id,
            'semester_id' => $request->semester_id,
            'mark_id'     => $request->mark_id,
            'date'        => $request->date,
        ]);
    }
}
