<?php

namespace App\DTOs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AddressDTO extends ObjectData
{
    public ?string $place_name;
    public ?string $specific_location;

    public static function fromRequest($request): self
    {
        return new self([
            'place_name'        => $request['place_name'],
            'specific_location' => $request['specific_location'],
        ]);
    }
    public static function fromUpdateRequest($request): self
    {
        return new self([
            'place_name'        => $request->place_name,
            'specific_location' => $request->specific_location,
        ]);
    }
}
