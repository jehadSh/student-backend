<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\DB;

class UniqueStudentGroup implements ValidationRule
{

    private $groupId;

    public function __construct($groupId)
    {
        $this->groupId = $groupId;
    }

    public function validate(string $attribute, mixed $value, \Closure $fail): void
    {
        // dd("aa");

        // Check if the combination of student_id and group_id already exists
        $exists = DB::table('group_students')
            ->where('student_id', $value)
            ->where('group_id', $this->groupId)
            ->exists();

        if ($exists) {
            $fail('The student is already assigned to this group.');
        }
    }
}
