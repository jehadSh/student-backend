<?php

namespace App\Traits;

use App\Filters\BaseSearch;
use App\Models\Scopes\OrderByDescScope;
use App\Models\Scopes\PaginateScope;
use Illuminate\Support\Facades\Log;


trait HasFilterTrait
{
    public function scopeFilter($query)
    {
        (new BaseSearch())->apply($query, new (self::class));
    }

    public function getFilterable()
    {
        return $this->filterable;
    }

    public function getModelRelations(): array
    {
        return $this->modelRelations ?? [];
    }

}
