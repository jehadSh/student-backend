<?php

namespace App\Traits;

use App\Models\Scopes\OrderByDescScope;
use App\Models\Scopes\PaginateScope;
use Illuminate\Support\Facades\Log;


trait PerPageTrait
{
    protected static function booted()
    {
        static::addGlobalScope(new PaginateScope());
        static::addGlobalScope(new OrderByDescScope());
    }

}
