<?php

namespace App\Traits;

use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Str;
use Symfony\Component\Console\Exception\InvalidArgumentException;

trait EnumFunctions
{
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
