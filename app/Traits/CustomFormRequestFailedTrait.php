<?php

namespace App\Traits;

trait CustomFormRequestFailedTrait
{

    /**
     * Handle a failed validation attempt.
     *
     * @return void
     *
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw new \Illuminate\Http\Exceptions\HttpResponseException(
            \App\Services\ApiResponseService::validateResponse($validator->errors())
        );
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     */
    protected function failedAuthorization()
    {
        throw new \App\Exceptions\UnauthorizedException();
    }


}
