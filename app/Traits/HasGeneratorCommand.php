<?php

namespace App\Traits;

use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Str;
use Symfony\Component\Console\Exception\InvalidArgumentException;

trait HasGeneratorCommand
{
    /**
     * The name of class being generated.
     *
     * @var string
     */
    private $className;

    /**
     * The name of class being generated.
     *
     * @var string
     */
    private $model;


    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $this->setClassName();

        $this->setModel();

        $path = $this->getPath($this->className);

        if ($this->alreadyExists($this->getNameInput()))
        {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($this->className));

        $this->info($this->type.' created successfully.');

        $this->line("<info>Created $this->type :</info> $this->className");
    }

    /**
     * Set class name
     *
     * @return  \App\Console\Commands\MakeModelServiceCommand
     */
    private function setClassName()
    {
        $name = pascalCase($this->argument('name'));

        $this->className = $this->parseName($name);

        return $this;
    }

    /**
     * Set class name
     *
     * @return  \App\Console\Commands\MakeModelServiceCommand
     */
    private function setModel()
    {
        $model = pascalCase($this->argument('model'));

        $this->model = $model;

        return $this;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        if(!$this->argument('name')){
            throw new InvalidArgumentException("Missing required argument model name");
        }

        $stub = parent::replaceClass($stub, $name);

        $stub = str_replace('__CLASS__', $this->argument('name'), $stub);

        $stub = str_replace('__MODEL__', $this->model, $stub);

        $stub = str_replace('__RECORD__', camelCase($this->model), $stub);

        return $stub;
    }

    /**
     *
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return base_path("app/stubs/$this->type.stub");
    }


    /**
     * Parse the name and format according to the root namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function parseName($name)
    {
        $rootNamespace = $this->laravel->getNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        if (str_contains($name, '/')) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->parseName($this->getDefaultNamespace(trim($rootNamespace, '\\')).'\\'.$name);
    }


    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return base_path().'/'.str_replace('\\', '/', $name).'.php';
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the class'],
            ['model', InputArgument::REQUIRED, 'The name of the model related'],
        ];
    }
}
