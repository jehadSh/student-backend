<?php

namespace App\Interfaces;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

interface IServiceBase
{
    public function all();

    public function index();

    public function show(Model $model);

    public function store(Request|FormRequest $request, mixed ...$args): Model|array;

    public function update(Request|FormRequest $request, Model $model, mixed ...$args): Model|array;

    public function destroy(Model $model): void;
}
