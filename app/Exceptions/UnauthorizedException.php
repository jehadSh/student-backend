<?php

namespace App\Exceptions;

use App\Services\ApiResponseService;
use Exception;
use Throwable;

class UnauthorizedException extends Exception
{
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function render($request){
        return ApiResponseService::unauthorizedResponse();
    }
}
