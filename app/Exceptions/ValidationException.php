<?php

namespace App\Exceptions;

use App\Services\ApiResponseService;
use Exception;
use Throwable;
use function Laravel\Prompts\error;

class ValidationException extends Exception
{
    private $errors = [];
    public function __construct(string $message = "", int $code = 0, $errors = [], ?Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    public function render($request)
    {
        return ApiResponseService::validateResponse($this->errors);
    }


}
