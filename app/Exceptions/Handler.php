<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {

        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof \Illuminate\Validation\ValidationException) {
            $e = new ValidationException(errors: $e->validator->errors()->all());
        }
        if ($e instanceof ModelNotFoundException) {
            $e = new \App\Exceptions\ModelNotFoundException('This: ' . $e->getModel() . ' with this: ' . (int)$e->getIds());
        }
        if ($e instanceof AuthenticationException) {
            $e = new \App\Exceptions\UnauthorizedException();
        }
        if ($e instanceof DeniedDeleteException){
            $e = new DeniedDeleteException(trans('response.failed'));
        }
        return parent::render($request, $e);
    }
}
