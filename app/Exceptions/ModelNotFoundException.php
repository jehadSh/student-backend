<?php

namespace App\Exceptions;

use App\Services\ApiResponseService;
use Exception;
use Throwable;

class ModelNotFoundException extends Exception
{
    public function __construct(string $message = "", int $code = 0,$model="", ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function render($request){
        return ApiResponseService::notFoundResponse($this->message.' '.trans('response.not_found'));
    }
}
