<?php

namespace App\Services\ModelsServices;

use App\DTOs\TeacherDTO;
use App\Models\Address;
use App\Models\Member;
use App\Models\Teacher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class TeacherService
{

    public function __construct(
        protected MemberService     $memberService,
        protected AddressService    $addressService,
        protected CallNumberService $callNumberService
    ) {}

    public function all(): Collection
    {
        return Teacher::with('member', 'member.address', 'member.callNumbers')->filter()->get();
    }

    public function index()
    {
        return Teacher::with('member', 'member.address', 'member.callNumbers')->filter()->paginate();
    }

    public function store(mixed $request)
    {
        if (isset($request->member_id)) {
            $member = Member::find($request->member_id);
        } else {
            $member = $this->memberService->store($request);
        }
        $teacherDTO = TeacherDTO::fromRequest($request->teacher);
        $teacher = $member->teacher()->create($teacherDTO->all());
        if (isset($request->address)) {
            $this->addressService->createManyAddress($request->address, $member);
        }
        if (isset($request->callNumbers)) {
            $this->callNumberService->createManyCallNumbers($request->callNumbers, $member);
        }
        return $teacher;
    }

    public function show(Model|Teacher $model): Model|Teacher
    {
        return $model;
    }

    public function updateAll(Request|FormRequest $request, Model|Teacher $model)
    {
        $member =   $this->memberService->update($request, $model->member);
        if (isset($request->teacher)) {
            $this->update($request->teacher, $model);
        }
        if (isset($request->address)) {
            $this->addressService->updateManyAddress($request->address, $member);
        }
        if (isset($request->callNumbers)) {
            $this->callNumberService->updateManyCall($request->callNumbers, $member);
        }
        return $model;
    }

    public function update(mixed $request, Model|Teacher $model): Teacher
    {
        $TeacherDTO = TeacherDTO::fromRequest($request);
        $TeacherDTO->mergeForUpdate($model);
        $model->update($TeacherDTO->all());
        return $model;
    }

    public function destroy(Model|Teacher $model): void
    {
        $model->delete();
    }
}
