<?php

namespace App\Services\ModelsServices;

use App\DTOs\StudentCheckInDTO;
use App\Models\Mark;
use App\Models\Student;
use App\Models\StudentCheckIn;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class StudentCheckInService
{
    public function __construct(protected SavedMarkService $savedMarkService) {}

    public function all(): Collection
    {
        return StudentCheckIn::filter()->get();
    }

    public function index()
    {
        return StudentCheckIn::with('semester', 'group', 'student.member')->filter()->paginate();
    }

    public function store(Request|FormRequest $request): StudentCheckIn
    {
        $StudentCheckInDTO = StudentCheckInDTO::fromRequest($request);
        $StudentCheckInData = $StudentCheckInDTO->all();
        $student = Student::find($StudentCheckInData['student_id']);
        $StudentCheckInData['group_id'] = $student->current_group_id;
        $StudentCheckInData['semester_id'] = $student->currentGroup->current_semester_id;
        $StudentCheckIns = StudentCheckIn::create($StudentCheckInData);

        $AttendanceMark = 1; // For The Attendance Mark.
        $mark = Mark::find($AttendanceMark)->first()->toArray();
        $this->savedMarkService->storeFromArray([
            'value'       => $mark["max_mark"],
            'student_id'  => $StudentCheckInData["student_id"],
            'semester_id' => $StudentCheckInData["semester_id"],
            'mark_id'     => $AttendanceMark,
            'date'        => $StudentCheckInData["date"],
        ]);

        return $StudentCheckIns->load('semester', 'group', 'student.member');
    }

    public function show(Model|StudentCheckIn $model): Model|StudentCheckIn
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|StudentCheckIn $model): StudentCheckIn
    {
        $StudentCheckInDTO = StudentCheckInDTO::fromRequest($request);
        $StudentCheckInDTO->mergeForUpdate($model);
        $model->update($StudentCheckInDTO->all());
        return $model;
    }

    public function destroy(Model|StudentCheckIn $model): void
    {
        $model->delete();
    }
}
