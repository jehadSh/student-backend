<?php

namespace App\Services\ModelsServices;

use App\DTOs\AddressDTO;
use App\Models\Address;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class AddressService
{
    public function all(): Collection
    {
        return Address::filter()->get();
    }

    public function index()
    {
        return Address::filter()->paginate();
    }

    public function store($request, $member): Address
    {
        $AddressDTO = AddressDTO::fromRequest($request);
        return $member->address()->create($AddressDTO->all());
    }
    public function storeFromUpdate($request, $member): Address
    {
        $AddressDTO = AddressDTO::fromUpdateRequest($request);
        return $member->address()->create($AddressDTO->all());
    }

    public function show(Model|Address $model): Model|Address
    {
        return $model;
    }
    public function updateManyAddress(array $addresses, $member)
    {
        Address::where('member_id', $member->id)->delete();
        foreach ($addresses as $address) {
            $this->storeFromUpdate((object)$address, $member);
        }
    }

    public function update(mixed $request, Model|Address $model): Address
    {
        $AddressDTO = AddressDTO::fromUpdateRequest($request);
        $AddressDTO->mergeForUpdate($model);
        $model->update($AddressDTO->all());
        return $model;
    }

    public function destroy(Model|Address $model): void
    {
        $model->delete();
    }

    public function createManyAddress(array $address, $member): void
    {
        foreach ($address as $addres) {
            $this->store($addres, $member);
        }
    }
}
