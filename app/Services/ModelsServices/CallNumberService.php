<?php

namespace App\Services\ModelsServices;

use App\DTOs\CallNumberDTO;
use App\Models\CallNumber;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class CallNumberService
{
    public function all(): Collection
    {
        return CallNumber::filter()->get();
    }

    public function index()
    {
        return CallNumber::filter()->paginate();
    }

    public function store($request, $member): CallNumber
    {
        $CallNumberDTO = CallNumberDTO::fromRequest($request);
        return $member->callNumbers()->create($CallNumberDTO->all());
    }
    public function storeFromUpdate($request, $member): CallNumber
    {
        $CallNumberDTO = CallNumberDTO::fromUpdateRequest($request);
        return $member->callNumbers()->create($CallNumberDTO->all());
    }

    public function show(Model|CallNumber $model): Model|CallNumber
    {
        return $model;
    }
    public function updateManyCall(array $calls, $member)
    {
        CallNumber::where('member_id', $member->id)->delete();

        foreach ($calls as $call) {
            $this->storeFromUpdate((object)$call, $member);
        }
    }

    public function update(mixed $request, Model|CallNumber $model): CallNumber
    {
        $CallNumberDTO = CallNumberDTO::fromUpdateRequest($request);
        $CallNumberDTO->mergeForUpdate($model);
        $model->update($CallNumberDTO->all());
        return $model;
    }

    public function destroy(Model|CallNumber $model): void
    {
        $model->delete();
    }

    public function createManyCallNumbers(array $callNumbers, $member): void
    {
        foreach ($callNumbers as $callNumber) {
            $this->store($callNumber, $member);
        }
    }
}
