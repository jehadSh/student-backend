<?php

namespace App\Services\ModelsServices;

use App\DTOs\SavedMarkDTO;
use App\Models\SavedMark;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class SavedMarkService
{
    public function all(): Collection
    {
        return SavedMark::with('student', 'semester', 'mark', 'student.member')->filter()->get();
    }

    public function index()
    {
        return SavedMark::with('student', 'semester', 'mark', 'student.member')->filter()->paginate();
    }

    public function store(Request|FormRequest $request): SavedMark
    {
        $SavedMarkDTO = SavedMarkDTO::fromRequest($request);

        $saveMark = SavedMark::create($SavedMarkDTO->all());
        return $saveMark->loadMissing('student', 'semester', 'mark', 'student.member');
    }

    public function storeFromArray(array $data)
    {
        SavedMark::create($data);
    }

    public function show(Model|SavedMark $model): Model|SavedMark
    {
        return $model->load('student', 'semester', 'mark', 'student.member');
    }

    public function update(Request|FormRequest $request, Model|SavedMark $model): SavedMark
    {
        $SavedMarkDTO = SavedMarkDTO::fromRequest($request);
        $SavedMarkDTO->mergeForUpdate($model);
        $model->update($SavedMarkDTO->all());
        return $model->load('student', 'semester', 'mark', 'student.member');
    }

    public function destroy(Model|SavedMark $model): void
    {
        $model->delete();
    }
}
