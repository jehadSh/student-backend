<?php

namespace App\Services\ModelsServices;

use App\DTOs\TimeTypeDTO;
use App\Models\TimeType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class TimeTypeService
{
    public function all(): Collection
    {
        return TimeType::filter()->get();
    }

    public function index()
    {
        return TimeType::filter()->paginate();
    }

    public function store(Request|FormRequest $request): TimeType
    {
        $TimeTypeDTO = TimeTypeDTO::fromRequest($request);
        return TimeType::create($TimeTypeDTO->all());
    }

    public function show(Model|TimeType $model): Model|TimeType
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|TimeType $model): TimeType
    {
        $TimeTypeDTO = TimeTypeDTO::fromRequest($request);
        $TimeTypeDTO->mergeForUpdate($model);
        $model->update($TimeTypeDTO->all());
        return $model;
    }

    public function destroy(Model|TimeType $model): void
    {
        $model->delete();
    }
}
