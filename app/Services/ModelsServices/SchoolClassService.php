<?php

namespace App\Services\ModelsServices;

use App\DTOs\SchoolClassDTO;
use App\Models\SchoolClass;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class SchoolClassService
{
    public function all(): Collection
    {
        return SchoolClass::filter()->get();
    }

    public function index()
    {
        return SchoolClass::filter()->paginate();
    }

    public function store(Request|FormRequest $request): SchoolClass
    {
        $SchoolClassDTO = SchoolClassDTO::fromRequest($request);
        return SchoolClass::create($SchoolClassDTO->all());
    }

    public function show(Model|SchoolClass $model): Model|SchoolClass
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|SchoolClass $model): SchoolClass
    {
        $SchoolClassDTO = SchoolClassDTO::fromRequest($request);
        $SchoolClassDTO->mergeForUpdate($model);
        $model->update($SchoolClassDTO->all());
        return $model;
    }

    public function destroy(Model|SchoolClass $model): void
    {
        $model->delete();
    }
}

