<?php

namespace App\Services\ModelsServices;

use App\DTOs\SemesterDTO;
use App\Models\Semester;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class SemesterService
{
    public function all(): Collection
    {
        return Semester::with('timeType')->filter()->get();
    }

    public function index()
    {
        return Semester::with('timeType')->filter()->paginate();
    }

    public function store(Request|FormRequest $request): Semester
    {
        $SemesterDTO = SemesterDTO::fromRequest($request);
        return Semester::create($SemesterDTO->all());
    }

    public function show(Model|Semester $model): Model|Semester
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|Semester $model): Semester
    {
        $SemesterDTO = SemesterDTO::fromRequest($request);
        $SemesterDTO->mergeForUpdate($model);
        $model->update($SemesterDTO->all());
        return $model;
    }

    public function destroy(Model|Semester $model): void
    {
        $model->delete();
    }
}

