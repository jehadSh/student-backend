<?php

namespace App\Services\ModelsServices;

use App\DTOs\MarkPackageDTO;
use App\Models\MarkPackage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class MarkPackageService
{
    public function all(): Collection
    {
        return MarkPackage::filter()->get();
    }

    public function index()
    {
        return MarkPackage::filter()->paginate();
    }

    public function store(Request|FormRequest $request): MarkPackage
    {
        $MarkPackageDTO = MarkPackageDTO::fromRequest($request);
        return MarkPackage::create($MarkPackageDTO->all());
    }

    public function show(Model|MarkPackage $model): Model|MarkPackage
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|MarkPackage $model): MarkPackage
    {
        $MarkPackageDTO = MarkPackageDTO::fromRequest($request);
        $MarkPackageDTO->mergeForUpdate($model);
        $model->update($MarkPackageDTO->all());
        return $model;
    }

    public function destroy(Model|MarkPackage $model): void
    {
        $model->delete();
    }
}

