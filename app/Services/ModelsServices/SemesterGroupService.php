<?php

namespace App\Services\ModelsServices;

use App\DTOs\SemesterGroupDTO;
use App\Models\SemesterGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class SemesterGroupService
{
    public function all(): Collection
    {
        return SemesterGroup::filter()->get();
    }

    public function index()
    {
        return SemesterGroup::filter()->paginate();
    }

    public function store(mixed $request): SemesterGroup
    {
        $SemesterGroupDTO = SemesterGroupDTO::fromRequest($request);
        return SemesterGroup::create($SemesterGroupDTO->all());
    }

    public function show(Model|SemesterGroup $model): Model|SemesterGroup
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|SemesterGroup $model): SemesterGroup
    {
        $SemesterGroupDTO = SemesterGroupDTO::fromRequest($request);
        $SemesterGroupDTO->mergeForUpdate($model);
        $model->update($SemesterGroupDTO->all());
        return $model;
    }

    public function destroy($group_id): void
    {
//        $model = SemesterGroup::where('group_id', $group_id)->delete();
//        $model->delete();
    }
}

