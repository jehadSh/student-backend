<?php

namespace App\Services\ModelsServices;

use App\DTOs\MarkDTO;
use App\Models\Mark;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class MarkService
{
    public function all(): Collection
    {
        return Mark::filter()->get();
    }

    public function index()
    {
        return Mark::with(Mark::$pluralRelationships)->filter()->paginate();
    }

    public function store(Request|FormRequest $request): Mark
    {
        $MarkDTO = MarkDTO::fromRequest($request);
        return Mark::create($MarkDTO->all());
    }

    public function show(Model|Mark $model): Model|Mark
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|Mark $model): Mark
    {
        $MarkDTO = MarkDTO::fromRequest($request);
        $MarkDTO->mergeForUpdate($model);
        $model->update($MarkDTO->all());
        return $model;
    }

    public function destroy(Model|Mark $model): void
    {
        $model->delete();
    }
}
