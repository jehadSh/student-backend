<?php

namespace App\Services\ModelsServices;

use App\DTOs\EducationLevelDTO;
use App\Models\EducationLevel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class EducationLevelService
{
    public function all(): Collection
    {
        return EducationLevel::filter()->get();
    }

    public function index()
    {
        return EducationLevel::filter()->paginate();
    }

    public function store(Request|FormRequest $request): EducationLevel
    {
        $EducationLevelDTO = EducationLevelDTO::fromRequest($request);
        return EducationLevel::create($EducationLevelDTO->all());
    }

    public function show(Model|EducationLevel $model): Model|EducationLevel
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|EducationLevel $model): EducationLevel
    {
        $EducationLevelDTO = EducationLevelDTO::fromRequest($request);
        $EducationLevelDTO->mergeForUpdate($model);
        $model->update($EducationLevelDTO->all());
        return $model;
    }

    public function destroy(Model|EducationLevel $model): void
    {
        $model->delete();
    }
}
