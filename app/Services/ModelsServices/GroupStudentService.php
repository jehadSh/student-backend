<?php

namespace App\Services\ModelsServices;

use App\DTOs\GroupStudentDTO;
use App\Models\Group;
use App\Models\GroupStudent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class GroupStudentService
{
    public function all(): Collection
    {
        return GroupStudent::with('group', 'student.member')->filter()->get();
    }

    public function index()
    {
        return GroupStudent::with('group', 'student.member')->filter()->paginate();
    }

    public function createMany(Request|FormRequest $request)
    {

        $group = Group::find($request->group_id);
        $temp = [];

        foreach ($request->students as $student) {
            $dto = [
                'student_id' => $student['id'],
                'join_date' => $student['join_date'],
                'group_id' => $group->id
            ];
            $temp[] = $this->store($dto);
        }
        return $temp;
    }

    public function store($dto)
    {
        $GroupStudentDTO = GroupStudentDTO::fromRequest($dto);
        $groupStudent =  GroupStudent::create($GroupStudentDTO->all());
        return $groupStudent->load('group', 'student.member');
    }

    public function show(Model|GroupStudent $model): Model|GroupStudent
    {
        return $model->load('group', 'student.member');
    }

    public function updateMany(Request|FormRequest $request)
    {
        $group = Group::find($request->group_id);
        $temp = [];

        foreach ($request->students as $student) {
            $dto = [
                'student_id' => $student['id'],
                'join_date' => $student['join_date'],
                'group_id' => $group->id
            ];
            $model =  GroupStudent::where('student_id', $student['id'])->where('group_id', $group->id)->first();
            if (isset($model)) {
                $temp[] = $this->update($dto, $model);
            } else {
                $temp[] = $this->store($dto);
            }
        }
        return $temp;
    }

    public function update(mixed $request, Model|GroupStudent $model): GroupStudent
    {
        $GroupStudentDTO = GroupStudentDTO::fromRequest($request);
        $model->update($GroupStudentDTO->all());
        return $model;
    }

    public function destroy(Model|GroupStudent $model): void
    {
        $model->delete();
    }
}
