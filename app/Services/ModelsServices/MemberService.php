<?php

namespace App\Services\ModelsServices;

use App\DTOs\MemberDTO;
use App\Interfaces\IServiceBase;
use App\Models\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class MemberService implements IServiceBase
{
    public function __construct() {}

    public function all(): Collection
    {
        return Member::with(Member::$pluralRelationships)->filter()->get();
    }

    public function index()
    {
        return Member::with(Member::$pluralRelationships)->filter()->paginate();
    }

    public function store(Request|FormRequest $request, mixed ...$args): Member|array
    {

        $memberDTo = MemberDTO::fromRequest($request);
        return Member::create($memberDTo->all());
    }

    public function show(Model|Member $model)
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model $model, mixed ...$args): Model
    {
        $memberDTO = MemberDTO::fromRequest($request);
        $memberDTO = $memberDTO->mergeForUpdate($model);
        $model->update($memberDTO->all());
        return $model;
    }

    public function destroy(Model|Member $model): void
    {
        $model->delete();
    }
}
