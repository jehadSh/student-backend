<?php

namespace App\Services\ModelsServices;

use App\DTOs\StudentDTO;
use App\Models\Member;
use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Mix;

class StudentService
{
    public function __construct(
        protected MemberService     $memberService,
        protected AddressService    $addressService,
        protected CallNumberService $callNumberService,

    ) {}

    public function all(): Collection
    {
        return Student::with('member', 'member.address', 'member.callNumbers', 'currentGroup')->filter()->get();
    }

    public function index()
    {
        return Student::with('member', 'member.address', 'member.callNumbers', 'currentGroup')->filter()->paginate();
    }

    public function store(mixed $request)
    {
        if (isset($request->member_id)) {
            $member = Member::find($request->member_id);
        } else {
            $member = $this->memberService->store($request);
        }
        $studentDTO = StudentDTO::fromRequest($request->student);
        $student = $member->student()->create($studentDTO->all());
        if (isset($request->address)) {
            $this->addressService->createManyAddress($request->address, $member);
        }
        if (isset($request->callNumbers)) {
            $this->callNumberService->createManyCallNumbers($request->callNumbers, $member);
        }
        return $student;
    }

    public function show(Model|Student $model): Model|Student
    {
        return $model;
    }
    public function updateAll(Request|FormRequest $request, Model|Student $model)
    {
        $member =   $this->memberService->update($request, $model->member);
        if (isset($request->student)) {
            $this->update($request->student, $model);
        }

        if (isset($request->address)) {
            $this->addressService->updateManyAddress($request->address, $member);
        }
        if (isset($request->callNumbers)) {
            $this->callNumberService->updateManyCall($request->callNumbers, $member);
        }
        return $model;
    }

    public function update(mixed $request, Model|Student $model): Student
    {
        $StudentDTO = StudentDTO::fromRequest($request);
        $StudentDTO->mergeForUpdate($model);
        $model->update($StudentDTO->all());
        return $model;
    }

    public function destroy(Model|Student $model): void
    {
        $model->delete();
    }
    public function allWithInActive()
    {
        return Student::withoutGlobalScope('active')->with('member', 'member.address', 'member.callNumbers', 'currentGroup')->filter()->get();
    }

    public function activeStudent($group, $request)
    {

        $this->makeAllStudentInActive($group);
        if (isset($request->ids)) {
            Student::withoutGlobalScope('active')->whereIn('id', $request->ids)
                ->update(['is_active' => 1]);
        }
    }
    public function makeAllStudentInActive($group)
    {
        Student::withoutGlobalScope('active')->where('current_group_id', $group->id)
            ->update(['is_active' => 0]);
    }
}
