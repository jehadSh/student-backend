<?php

namespace App\Services\ModelsServices;

use App\DTOs\GroupDTO;
use App\Models\Group;
use App\Models\Mark;
use App\Models\SavedMark;
use App\Models\Student;
use App\Models\StudentCheckIn;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class GroupService
{

    public function __construct(
        protected SemesterGroupService $semesterGroupService,
        protected StudentService       $studentService,

    )
    {
    }

    public function all(): Collection
    {
        return Group::with('schoolClass', 'teacher', 'teacher.member', 'current_semester')->filter()->get();
    }

    public function all_with_trashed(): Collection
    {
        return Group::withoutGlobalScope('active')->with('schoolClass', 'teacher', 'teacher.member', 'current_semester')->filter()->get();
    }

    public function index()
    {
        return Group::with('schoolClass', 'teacher', 'teacher.member', 'current_semester')->filter()->paginate();
    }

    public function store(Request|FormRequest $request): Group
    {
        $GroupDTO = GroupDTO::fromRequest($request);
        $group = Group::create($GroupDTO->all());
        $data = [
            'group_id'    => $group->id,
            'semester_id' => $request->current_semester_id,
        ];
        $this->semesterGroupService->store($data);
        return $group;
    }

    public function show(Model|Group $model): Model|Group
    {
        return $model;
    }

    public function update(Request|FormRequest $request, Model|Group $model): Group
    {
        if ($model->current_semester_id == $request->current_semester_id && isset($request->current_semester_id)) {
            $data = [
                'group_id'      => $model->id,
                'semester_id'   => $request->current_semester_id,
                'serial_number' => $request->serial_number,
            ];
            $this->semesterGroupService->store($data);
            $this->studentService->makeAllStudentInActive($model);
        }
        $GroupDTO = GroupDTO::fromRequest($request);
        $GroupDTO->mergeForUpdate($model);
        $model->update($GroupDTO->all());
        return $model;
    }

    public function destroy(Model|Group $model): void
    {
        $model->update([
            'is_active' => 0
        ]);
        //        $this->semesterGroupService->destroy($model->id);
        //        $model->delete();
    }

    public function AssignStudentToGroup($request, $group)
    {
        $studentIds = json_decode($request->studentId);
        // Sync the students without detaching
        $pivotData = [];
        foreach ($studentIds as $studentId) {
            // Assuming $request->join_date is a valid date string
            $pivotData[$studentId] = ['join_date' => $request->join_date];
        }
        $group->students()->syncWithoutDetaching($pivotData);

        return $group;
    }

    public function giveGroupReward($request, $group)
    {
        $studentThisGroup = Student::where('current_group_id', $group->id)
            ->get();

        $checkIns = StudentCheckIn::whereIn('student_id', $studentThisGroup->pluck('id'))
            ->where('date', $request->date)
            ->where('is_exist', true)
            ->get();

        $mark = Mark::where('id', $request->mark_id)->first();

        $marksData = [];

        foreach ($checkIns as $checkIn) {
            $marksData[] = [
                'value'       => $mark->max_mark,
                'date'        => $request->date,
                'student_id'  => $checkIn->student_id,
                'mark_id'     => $request->mark_id,
                'semester_id' => $group->current_semester_id,
            ];
        }

        SavedMark::insert($marksData);

        return true;
    }
}
