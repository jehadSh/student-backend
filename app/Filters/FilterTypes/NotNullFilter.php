<?php

namespace App\Filters\FilterTypes;

use PhpParser\Builder;

class NotNullFilter extends BaseFilter
{
    public function __invoke($query, $field, $value)
    {
        $this->apply($query, $field, $value);
    }

    function apply($query, $field, $value)
    {
        return $query->whereNotNull($field);
    }
}
