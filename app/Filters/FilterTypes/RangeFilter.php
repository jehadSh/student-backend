<?php

namespace App\Filters\FilterTypes;

use PhpParser\Builder;

class RangeFilter extends BaseFilter
{
    public function __invoke($query, $field, $value)
    {
        $this->apply($query, $field, $value);
    }

    function apply($query, $field, $value)
    {
        $arr = explode(',', $value);
        if (count($arr) == 1) {
            $filter = new SpecificTextFilter();
            return $filter($query, $field, $value);
        }
        sort($arr);

        return $query->whereBetween($field, $arr);
    }
}
